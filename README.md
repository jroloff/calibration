## Calibration

This package takes a list of jets and finds their calibrated pT using numerical inversion. Plots showing the response, resolution, and fits are made, before and after calibration. 

There are two ways to run this code, one of which allows the possibility of creating plots of efficiency and fake rate. This does require that the jet collection contains #all# reconstructed jets, or at least those above a certain threshold. It also requires that the ntuple contains a list of all truth jets, whether or not they have been matched to a reconstructed jet.

## Setup

Getting the code:

```
git clone https://gitlab.com/jroloff/calibration.git 
make clean
make
```

Running the code:
In run.sh, change the NPV range and pT range as desired. 

```
ls filenames*.root > inputs.txt
source run.sh
```


## Inputs


If efficiency plots are not being made, there are 2 required branches (for each set of jets that you want to calibrate).

1. Jets: A list of all reconstructed jets that should be calibrated. These can include non-isolated jets as well as jets not matched to truth jets, as these will be checked before finding a calibration.

2. Truth: A list of all truth jets matched to the reconstructed jets. If a reco jet does not have a truth jet associated with it, the truth jet should either be the closest jet or have a 4-momentum of (0,0,0,0). 

If efficiency plots are being made, an additional branch is required which is a list of all the truth jets.

All variables can be either a vector of TLorentzVectors or 4 branches with the pT, eta, phi, and E of each jet.

## Required Parameters


--filename: The input root file

--input: The name of the branch that contains the jets. You can have several entries for input. The form of the branches should be **input**Jets and **input**Truth in the case of using TLorentzVectors, or **input**JetsPt, ... in the case of specifying each part of the 4-momentum in a separate branch.  

--inputname: The name that should appear in the legend for that branch. Note that currently this should not contain any spaces

--minPt: The lowest pT of jets that should be calibrated

--maxPt: The max pT of jets that should be calibrated

--nPtBins: The binning in pT for jets

--minNPV: The lowest NPV

--maxNPV: The highest NPV

--nNPVBins: The number of bins in NPV

--minResponse: Minimum value for plotting response

--maxResponse: Maximum value for plotting response

--nResponseBins: Number of bins in response histogram

--doEfficiency: True if the efficiency plots should be made

--doFakeRate: True if the fake rate plots should be made

## Additional Parameters (Default values in parantheses)
--maxEta (0.8): The maximum eta of reco jets to be considered

--isolationSize (0.6): The required angular distance between jets when calculating isolation    

--treename (nominal): The name of the tree in the root file 

--isTLorentz (true): True if root file branches are TLorentzVectors

--maxentries (-1): The number of entries to run over. If maxentries == -1, all events are run over

--efficiencyPtCut: The minimum (calibrated) pT to be considered when calculating the efficiency of reconstructing truth jets

--isEM: True if the jets are EM-scale (for plotting purposes)

--isoPtCut (2): The lowest pT jets considered when calculating reco isolation

--doTruthIsolation (true): True if truth jets should be isolated

--truthIsoPtCut (5): The lowest pT of truth jets considered when calculating truth isolation

--nSigma (2): Used to determine range of fit

--fracCutoff: Used to determine range of fit



An example run file (run.sh) has been included to show how this can be used.


## Definitions

**Fake Rate**: The number of jets not matched to truth jets whose calibrated pT > fakeRatePtCut. Note that this currently does not count fakes who are matched to a truth that another reco jet is matched to

**Efficiency**: The percentage of (isolated) truth jets which have at least one reco jet matched to them, whose calibrated pT > efficiencyPtCut. Note that reco jets do not have to be isolated for this.

**Fit Resolution**: The Gaussian width of the response function, which has been fit with a Gaussian. Note that since response distributions frequently have non-Gaussian tails, and can be cut off (i.e. when a pT cutoff is applied to jets), only the core of the distribution is fitted. Also note that although resolution plots are made before and after calibration, only calibrated resolution plots are valid.

**IQR Resolution**: The right-hand side inter-quantile range the response distribution. In computing this, only the distribution which is "right" of the fitted mean is used, to avoid problems with pT cutoffs on the left. To match the fitted resolution, IQR resolution is defined as the width encompassing 68% of the RHS distribution. Given a Gaussian distribution, this should be the same as the fit resolution. 












