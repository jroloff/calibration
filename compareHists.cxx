#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <getopt.h>

#include "TCanvas.h"
#include "TAxis.h"
#include "TLegend.h"
#include "TPad.h"
#include "TH1.h"
#include "TColor.h"
#include "TString.h"
#include "TStyle.h"
#include "TFile.h"

#include "AtlasUtils.h"
#include "AtlasStyle.h"
#include "AtlasLabels.h"

using namespace std;

pair<double,double> getRange(vector<TH1F*> hists, bool isRatio);
void makeLegends(bool isRatio);
void makeLabels(bool isRatio);
vector<TString> getColors();
TH1F* convertGraphToHist(TGraphErrors* graph, int nBins, double minVal, double maxVal);

void drawHists(string fileName, vector<TH1F*> hists, bool hasErr);
void drawRatio(string fileName, vector<TH1F*> hists, vector<TH1F*> ratios, bool hasErr);

void getHists(string fileName, vector<string> avivFiles, string AvivHistName, string JenniferHistName, string xAxis, string yAxis, int nBins, double minBin, double maxBin);
void getRatio(string fileName, vector<string> avivFiles, string AvivHistName, string JenniferHistName, string xAxis, string yAxis, int nBins, double minBin, double maxBin);

vector<string> fitTitles, fitLegends;
vector<string> jenniferTitles, jenniferLegends;
vector<string> jenniferFiles;
vector<string> avivTitles, avivLegends;
vector<string> avivAIQRFiles;
vector<string> avivFitFiles;
string directory;
int isEM;


void drawHists(string fileName, vector<TH1F*> hists, bool hasErr){
  TCanvas *c1 = new TCanvas(Form("c1%s%s", directory.c_str(), fileName.c_str()),"c1", 800,600);

  vector<TString> colors = getColors();

  pair<double, double> minmax = getRange(hists, false);

  for(unsigned int j=0; j < hists.size(); j++){
		hists[j]->UseCurrentStyle();
    hists[j]->GetYaxis()->SetRangeUser(minmax.first, minmax.second);
    hists[j]->SetMarkerColor(TColor::GetColor(colors[j]));
    hists[j]->SetLineColor(TColor::GetColor(colors[j]));
		if(hasErr == true){
    	if(j==0) hists[j]->Draw("Ex0C hist");
    	else hists[j]->Draw("Ex0C hist SAME");
    	hists[j]->Draw("Lhist SAME");
		}
		else{
      if(j==0) hists[j]->Draw("LPhist");
      else hists[j]->Draw("LPhist SAME");
		}
  } //Get all info for different types of subtraction
  makeLabels(false);
  makeLegends(false);
  c1->Print(Form("%s/Summary%s.pdf",directory.c_str(), fileName.c_str()));
  c1->Print(Form("%s/Summary%s.eps",directory.c_str(), fileName.c_str()));
  c1->Print(Form("%s/Summary%s.png",directory.c_str(), fileName.c_str()));
}

void drawRatio(string fileName, vector<TH1F*> hists, vector<TH1F*> ratios, bool hasErr){
   TCanvas *c2 = new TCanvas(Form("c2%s%s", directory.c_str(), fileName.c_str()),"c2", 800,600);
   c2->cd();
   TPad *pad1 = new TPad("pad1", "pad1",0.05,0.293,0.95,0.95);
   pad1->SetTopMargin(0.01); // Upper and lower plot are joined
   pad1->SetBottomMargin(0.0025); // Upper and lower plot are joined
   pad1->SetLeftMargin(0.1);
   pad1->SetRightMargin(0.01);
   pad1->Draw();             // Draw the upper pad: pad1

   c2->cd();
   TPad *pad2 = new TPad("pad2", "pad2", 0.05,0.05,0.95,0.31);
   pad2->SetBottomMargin(0.35);
   pad2->SetTopMargin(0.0025); // Upper and lower plot are joined
   pad2->SetLeftMargin(0.1);
   pad2->SetRightMargin(0.01);
   pad2->Draw();

  vector<TString> colors = getColors();

  pair<double, double> minmax = getRange(hists, false);
  pair<double, double> minmaxRatios = getRange(ratios, true);

  c2->cd();
    for(unsigned int fit=0; fit<fitTitles.size(); fit++){
      pad1->cd();
      hists[fit]->GetYaxis()->SetRangeUser(minmax.first, minmax.second*1.08);
      hists[fit]->GetXaxis()->SetLabelSize(0);
      hists[fit]->GetYaxis()->SetTitleSize(.05);
      hists[fit]->GetYaxis()->SetLabelSize(.045);
      hists[fit]->GetYaxis()->SetTitleOffset(0.9);
      hists[fit]->SetMarkerColor(TColor::GetColor(colors[fit]));
      hists[fit]->SetLineColor(TColor::GetColor(colors[fit]));
      if(hasErr == true){
        if(fit == 0)hists[fit]->Draw("Ex0C hist");
        else hists[fit]->Draw("Ex0C hist SAME");
        hists[fit]->Draw("LPhist SAME");
      }
      else{
        if(fit == 0)hists[fit]->Draw("LPhist");
        else hists[fit]->Draw("LPhist SAME");
      }
      pad2->cd();
      ratios[fit]->GetYaxis()->SetRangeUser(minmaxRatios.first, minmaxRatios.second);
      ratios[fit]->GetXaxis()->SetTitleSize(0.12);
      ratios[fit]->GetYaxis()->SetTitleSize(0.12);
      ratios[fit]->GetYaxis()->SetLabelSize(0.12);
      ratios[fit]->GetYaxis()->SetLabelOffset(0.007);
      ratios[fit]->GetXaxis()->SetTickLength(0.08);
      ratios[fit]->GetXaxis()->SetLabelOffset(0.04);
      ratios[fit]->GetYaxis()->SetTitleOffset(0.30);
      ratios[fit]->GetXaxis()->SetLabelSize(0.12);
      ratios[fit]->GetYaxis()->SetNdivisions(4, 2, 2);
      ratios[fit]->GetXaxis()->SetNdivisions(10, 4, 2);
      ratios[fit]->GetYaxis()->SetTitle("Ratio to Area Sub.");
      ratios[fit]->SetMarkerColor(TColor::GetColor(colors[fit]));
      ratios[fit]->SetLineColor(TColor::GetColor(colors[fit]));

      if(fit == 0) ratios[fit]->Draw("Ex0C hist");
      else ratios[fit]->Draw("Ex0C hist SAME");
      ratios[fit]->Draw("LPhist SAME");

    }
    pad1->cd();

  makeLabels(true);
  makeLegends(true);

  c2->Print(Form("%s/Summary%s.pdf",directory.c_str(), fileName.c_str()));
  c2->Print(Form("%s/Summary%s.eps",directory.c_str(), fileName.c_str()));
  c2->Print(Form("%s/Summary%s.png",directory.c_str(), fileName.c_str()));

}

int main(int argc,char *argv[]){
  SetAtlasStyle();
  gStyle->SetMarkerStyle(20);
  gStyle->SetMarkerSize(1);


	string responseStr = "p_{t}^{reco} / p_{t}^{true}";
	string resolutionStr =  "#sigma(p_{t}^{reco} / p_{t}^{true})";
	string aiqrStr = "aIQR";
	string efficiencyStr = "Reconstruction Efficiency";
	string fakeStr = "Avg. Number of Pileup Jets";
	string ptTrueStr = "p_{t}^{true} [GeV]";


	string fileName = " ";
	int minNPV, maxNPV;
	int nPtBins = 8;
	double minPt = 20;
	double maxPt = 60;
	int nNPVBins = 4;
	int makeRatioPlots = true;
	isEM = 0;

	static struct option long_options[] =
	{   
    {"directory", 1, NULL, 'a'},
    {"makeRatioPlots", 1, NULL, 'b'},
    {"minNPV", 1, NULL, 'd'},
    {"maxNPV", 1, NULL, 'e'},
    {"nNPVBins", 1, NULL, 'f'},
    {"isEM", 1, NULL, 'g'},
    {"input", 1, NULL, 's'},
    {NULL, 0, NULL, 0}
	};

  int opt;
  while ( (opt = getopt_long(argc, argv,"abcdefghijkopqrstuvwxyz", long_options, NULL)) != -1 ) {  // for each option...
    switch ( opt ) {
      case 'a': directory = optarg; break;
      case 'b': makeRatioPlots = atoi(optarg); break;
      case 'd': minNPV = atoi(optarg); break;
      case 'e': maxNPV = atoi(optarg); break;
      case 'f': nNPVBins = atoi(optarg); break;
      case 'g': isEM = atoi(optarg); break;
      case 's': fileName = optarg; break;
      case 0: break;
    }
  }

  ifstream file(fileName.c_str());
	string person, fitFile, aiqrFile, plotName, legendName;

  while (file >> person >> fitFile >> aiqrFile >> plotName >> legendName){
		if(strcmp(person.c_str(), "Jennifer") == 0){
			jenniferFiles.push_back(fitFile);
			jenniferTitles.push_back(plotName);
			jenniferLegends.push_back(legendName);
		}
		if(strcmp(person.c_str(), "Aviv") == 0){
			avivFitFiles.push_back(fitFile);
			avivAIQRFiles.push_back(aiqrFile);
			avivTitles.push_back(plotName);
			avivLegends.push_back(legendName);
		}
  }

	getHists("Response", avivFitFiles, "jetclosure_pttrue_NPVincl", "CalibResponseInclusive", ptTrueStr, responseStr, nPtBins, minPt, maxPt);
	getHists("ResolutionFit", avivFitFiles, "jetsigmaR_pttrue_NPVincl", "CalibRelativeResolutionFitInclusive", ptTrueStr, resolutionStr, nPtBins, minPt, maxPt);
	getHists("ResolutionAIQR", avivAIQRFiles, "jetsigmaR_pttrue_NPVincl", "CalibRelativeResolutionAIQRInclusive", ptTrueStr, aiqrStr, nPtBins, minPt, maxPt);
	getHists("Efficiency", avivFitFiles, "jetefficiency_pttrue_NPVincl", "EfficiencyInclusive", ptTrueStr, efficiencyStr, nPtBins, minPt, maxPt);
	getHists("FakeRate", avivFitFiles, "fakejets_NPV", "nFakeJets", ptTrueStr, fakeStr, nNPVBins, minNPV, maxNPV);

	
	if(makeRatioPlots == 1){
		getRatio("ResolutionFitRatio", avivFitFiles, "jetsigmaR_pttrue_NPVincl","CalibRelativeResolutionFitInclusive", ptTrueStr, resolutionStr, nPtBins, minPt, maxPt);
		getRatio("ResolutionAIQRRatio", avivAIQRFiles, "jetsigmaR_pttrue_NPVincl", "CalibRelativeResolutionAIQRInclusive", ptTrueStr, aiqrStr, nPtBins, minPt, maxPt);
	}
}


pair<double,double> getRange(vector<TH1F*> hists, bool isRatio){
  double maxVal = -100;
  double minVal = 1000;
  for(unsigned int fit=0; fit<hists.size(); fit++){
     if(hists[fit]->GetMaximum() > maxVal) maxVal = hists[fit]->GetMaximum();
     if(hists[fit]->GetMinimum() < minVal) minVal = hists[fit]->GetMinimum();
  }

  double diff = maxVal - minVal;
	if(isRatio == false){
  	maxVal += diff*0.35;
  	minVal -= diff*0.10;

  	//Consistent scale for closure plots
	  if(minVal > 0.9 && maxVal < 1.1) {
  	  minVal = 0.9;
    	maxVal = 1.1;
  	}
	}

	if(isRatio == true){
		maxVal += diff*0.1;
		minVal -= diff*0.1;
	}

  pair<double,double> minmax(minVal, maxVal);
  return minmax;
}

void makeLegends(bool isRatio){
	vector<TString> colors = getColors();
  if(isRatio == true) gStyle->SetTextSize(0.06);
  for(unsigned int fit=0; fit<fitLegends.size(); fit++){
    if(isRatio == false){
      double Y = .90 - fit*0.04;
      myMarkerText( 0.7, Y, TColor::GetColor(colors[fit]), 20, fitLegends[fit].c_str(),1.3);
    }
    if(isRatio == true){
      double Y = .90 - fit*0.06;
      myMarkerText( 0.7, Y, TColor::GetColor(colors[fit]), 20, fitLegends[fit].c_str(),1.3);
    }
  }
  if(isRatio == true) gStyle->SetTextSize(0.04);
}


void makeLabels(bool isRatio){
  if(isRatio == true){
    gStyle->SetTextSize(0.06);
    ATLASLabel(.15, .88, "Simulation Internal",1);
    myText( 0.15, 0.82, 1, "Pythia Dijet #sqrt{s}= 13 TeV");
    if(isEM == 0)myText( 0.15, 0.76, 1, "anti-k_{t} LCW, R=0.4");
    else myText( 0.15, 0.76, 1, "anti-k_{t} EM, R=0.4");
    gStyle->SetTextSize(0.04);
  }

  if(isRatio == false){
    gStyle->SetTextSize(0.04);
    ATLASLabel(.2, .86, "Simulation Internal",1);
    myText( 0.2, 0.82, 1, "Pythia Dijet #sqrt{s}= 13 TeV");
    if(isEM == 0)myText( 0.2, 0.78, 1, "anti-k_{t} LCW, R=0.4");
    else myText( 0.2, 0.78, 1, "anti-k_{t} EM, R=0.4");
  }
}


TH1F* convertGraphToHist(TGraphErrors* graph, int nBins, double minVal, double maxVal){
  TH1F *tmp = new TH1F("tmp", "", nBins, minVal, maxVal);

  for(int i=0; i<nBins; i++){
    float val = graph->GetY()[i];
    double valErr = graph->GetErrorY(i);
    tmp->SetBinContent(i+1, val);
    tmp->SetBinError(i+1, valErr);
  }

  return tmp;
}



vector<TString>  getColors(){
	vector<TString> colors;
  colors = vector<TString>(8);
  colors[0] = "#3eada2";
  colors[1] = "#553366";
  colors[2] = "#ee8573";
  colors[3] = "#669900";
  colors[4] = "#ffbe3b";
  colors[5] = "#035857";
  colors[6] = "#990000";
  colors[7] = "#bcbc93";
	return colors;
}


void getHists(string fileName, vector<string> avivFiles, string AvivHistName, string JenniferHistName, string xAxis, string yAxis, int nBins, double minBin, double maxBin){
  vector<TH1F*> hists;
	fitTitles.clear();
	fitLegends.clear();

  for(unsigned int i=0; i<avivFiles.size(); i++){
    TFile *file = new TFile(Form("%s", avivFiles[i].c_str()));
    TGraphErrors *g_tmp1 = (TGraphErrors*)file->Get(Form("%s", AvivHistName.c_str()));
    TH1F *h_tmp1 = convertGraphToHist(g_tmp1, nBins, minBin, maxBin);
    h_tmp1->GetXaxis()->SetTitle(xAxis.c_str());
    h_tmp1->GetYaxis()->SetTitle(yAxis.c_str());		
    hists.push_back(h_tmp1);
    fitTitles.push_back(avivTitles[i]);
    fitLegends.push_back(avivLegends[i]);
  }

  for(unsigned int i=0; i<jenniferFiles.size(); i++){
    TFile *file = new TFile(Form("%s", jenniferFiles[i].c_str()));
    TH1F *h_tmp = (TH1F*) file->Get(Form("h_%s_%s", jenniferTitles[i].c_str(), JenniferHistName.c_str()));
    h_tmp->GetXaxis()->SetTitle(xAxis.c_str());
    h_tmp->GetYaxis()->SetTitle(yAxis.c_str());		
    hists.push_back(h_tmp);
    fitTitles.push_back(jenniferTitles[i]);
    fitLegends.push_back(jenniferLegends[i]);
  }

  drawHists(fileName, hists, true);
}


void getRatio(string fileName, vector<string> avivFiles, string AvivHistName, string JenniferHistName, string xAxis, string yAxis, int nBins, double minBin, double maxBin){
  vector<TH1F*> hists, ratios;
	fitTitles.clear();
	fitLegends.clear();

  for(unsigned int i=0; i<avivFiles.size(); i++){
    TFile *file = new TFile(Form("%s", avivFiles[i].c_str()));
    TGraphErrors *g_tmp1 = (TGraphErrors*)file->Get(Form("%s", AvivHistName.c_str()));
    TH1F *h_tmp1 = convertGraphToHist(g_tmp1, nBins, minBin, maxBin);
    h_tmp1->GetXaxis()->SetTitle(xAxis.c_str());
    h_tmp1->GetYaxis()->SetTitle(yAxis.c_str());		

    hists.push_back(h_tmp1);
    fitTitles.push_back(avivTitles[i]);
    fitLegends.push_back(avivLegends[i]);
  }

  for(unsigned int i=0; i<jenniferFiles.size(); i++){
    TFile *file = new TFile(Form("%s", jenniferFiles[i].c_str()));
    TH1F *h_tmp = (TH1F*) file->Get(Form("h_%s_%s", jenniferTitles[i].c_str(), JenniferHistName.c_str()));
    TH1F *h_tmpRat = (TH1F*) file->Get(Form("h_%s_%sRatioClone", jenniferTitles[i].c_str(), JenniferHistName.c_str()));
    h_tmp->GetXaxis()->SetTitle(xAxis.c_str());
    h_tmp->GetYaxis()->SetTitle(yAxis.c_str());		
    h_tmpRat->GetXaxis()->SetTitle(xAxis.c_str());
    h_tmpRat->GetYaxis()->SetTitle(yAxis.c_str());		
    hists.push_back(h_tmp);
    ratios.push_back(h_tmpRat);
    fitTitles.push_back(jenniferTitles[i]);
    fitLegends.push_back(jenniferLegends[i]);
  }

  drawRatio(fileName, hists, ratios, true);
}
