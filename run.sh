#/bin/bash

./main --filename lowMuAll.root \
	--input Calib \
	--inputname AreaSub \
	--input Voronoi \
	--inputname Voronoi \
	--input SK4Voronoi \
	--inputname Voronoi+SK0.4 \
	--input SK6Voronoi \
	--inputname Voronoi+SK0.6 \
	--minNPV  5 \
	--maxNPV 25 \
	--nNPVBins 4 \
	--minPt 20 \
	--maxPt 60 \
	--nPtBins 8 \
	--doEfficiency 1 \
	--doFakeRate 1 \
	--nResponseBins 80 \
	--recoPtCut 4 \
	--isEM 0 \
  --nSigma 2 \
  --fracCutoff .3 \
	--maxentries 50000 \
	--efficiencyPtCut 20

