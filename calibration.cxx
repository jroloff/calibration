#include "calibration.h"
#include "TSystem.h"
#include <fstream>

using namespace std;


calibration::calibration(vector<string> names, vector<string> titles, double nevt, unsigned int npvbins, double minnpv, double maxnpv, unsigned int ptbins, double minpt, double maxpt, double efficiencyCut, int nresponsebins, double minresponse, double maxresponse, int isem, double nsigma, double fraccutoff, double isoptcut){

  SetAtlasStyle();
	f = new TFile("outputHists.root","RECREATE");
	path = gSystem->pwd();
	nentries = nevt;
	makeColors();
	
	nNPVBins = npvbins;
	minNPV = minnpv;
	maxNPV = maxnpv;

	nPtBins = ptbins;
	minPt = minpt;
	maxPt = maxpt;

	isEM = isem;

	nSigma = nsigma;
	fracCutoff = fraccutoff;

  minResponse = minresponse;
  maxResponse = maxresponse;
  nResponseBins = nresponsebins;


	isoPtCut = isoptcut;
	efficiencyCalibPtCut = efficiencyCut;

  c1 = new TCanvas("c1","c1", 0., 0., 800,600);

   c2 = new TCanvas("c2","c2", 800,600);
   c2->cd();
   pad1 = new TPad("pad1", "pad1",0.05,0.293,0.95,0.95);
   pad1->SetTopMargin(0.01); // Upper and lower plot are joined
   pad1->SetBottomMargin(0.0025); // Upper and lower plot are joined
   pad1->SetLeftMargin(0.1);
   pad1->SetRightMargin(0.01);
	 pad1->Draw();             // Draw the upper pad: pad1

   c2->cd();
   pad2 = new TPad("pad2", "pad2", 0.05,0.05,0.95,0.31);
   pad2->SetBottomMargin(0.35);
   pad2->SetTopMargin(0.0025); // Upper and lower plot are joined
   pad2->SetLeftMargin(0.1);
   pad2->SetRightMargin(0.01);
   pad2->Draw();


	gStyle->SetMarkerStyle(20);
	gStyle->SetMarkerSize(2);
	gStyle->SetTitleSize(0.04, "X");
	gStyle->SetLabelSize(0.04, "X");
	gStyle->SetTitleSize(0.04, "Y");
	gStyle->SetLabelSize(0.04, "Y");

	for(unsigned int i=0; i<names.size(); i++)		fitNames.push_back(names[i]);
	for(unsigned int i=0; i<titles.size(); i++)		titleNames.push_back(titles[i]);

	responseString = "p_{t}^{reco} / p_{t}^{truth}";
	resolutionString = "#sigma(p_{t}^{reco} / p_{t}^{truth})";
	iqrString = "RHS IQR";
	aiqrString = "Absolute RHS IQR";

	sumWeightNPV = vector<double>(nNPVBins, 0);

	dataPt = vector<vector<vector<vector<double> > > >(fitNames.size(), vector<vector<vector<double> > >(nNPVBins, vector<vector<double> >(nPtBins, vector<double>(0))));
	dataPtTrue = vector<vector<vector<vector<double> > > >(fitNames.size(), vector<vector<vector<double> > >(nNPVBins, vector<vector<double> >(nPtBins, vector<double>(0))));
	dataIsolation = vector<vector<vector<vector<bool> > > >(fitNames.size(), vector<vector<vector<bool> > >(nNPVBins, vector<vector<bool> >(nPtBins, vector<bool>(0))));
	dataWeight = vector<vector<vector<vector<double> > > >(fitNames.size(), vector<vector<vector<double> > >(nNPVBins, vector<vector<double> >(nPtBins, vector<double>(0))));

	NonIsoDataPt = vector<vector<vector<vector<double> > > >(fitNames.size(), vector<vector<vector<double> > >(nNPVBins, vector<vector<double> >(nPtBins, vector<double>(0))));
	NonIsoDataPtTrue = vector<vector<vector<vector<double> > > >(fitNames.size(), vector<vector<vector<double> > >(nNPVBins, vector<vector<double> >(nPtBins, vector<double>(0))));
	NonIsoDataWeight = vector<vector<vector<vector<double> > > >(fitNames.size(), vector<vector<vector<double> > >(nNPVBins, vector<vector<double> >(nPtBins, vector<double>(0))));

	fakeDataPt = vector<vector<vector<double> > >(fitNames.size(), vector<vector<double> >(nNPVBins, vector<double>(0)));
	fakeDataWeight = vector<vector<vector<double> > >(fitNames.size(), vector<vector<double> >(nNPVBins, vector<double>(0)));
	truthDataWeight = vector<vector<vector<double> > >(fitNames.size(), vector<vector<double> >(nNPVBins, vector<double>(0)));

	allDataPt = vector<vector<vector<double> > >(fitNames.size(), vector<vector<double> >(nNPVBins, vector<double>(0)));
	allDataWeight = vector<vector<vector<double> > >(fitNames.size(), vector<vector<double> >(nNPVBins, vector<double>(0)));

	truthMatchedReco = vector<vector<vector<vector<double> > > >(fitNames.size(), vector<vector<vector<double> > >(nNPVBins, vector<vector<double> >(nPtBins, vector<double>(0))));
	truthMatchedWeight = vector<vector<vector<vector<double> > > >(fitNames.size(), vector<vector<vector<double> > >(nNPVBins, vector<vector<double> >(nPtBins, vector<double>(0))));

	truthAllWeight = vector<vector<vector<double> > >(fitNames.size(), vector<vector<double> >(nNPVBins, vector<double>(nPtBins, 0)));
	truthAllWeightInclusive = vector<vector<double> >(fitNames.size(), vector<double>(nPtBins, 0));
	truthMatchedRatio = vector<vector<vector<double> > >(fitNames.size(), vector<vector<double> >(nNPVBins, vector<double>(nPtBins, 0)));

	RecoFits = vector<vector<TF1*> >(fitNames.size(), vector<TF1*>(nNPVBins,  0));
}


void calibration::doCalibration(){
	makeCalibratedPlots("", false);
	makeCalibrationCurves();
	makeCalibratedPlots("Calib", true);
}

void calibration::makeCalibratedPlots(string calibType, bool isCalib){
	unsigned int nFitNames = fitNames.size();

  avgResponse = vector<vector<vector<double> > >(fitNames.size(), vector<vector<double> >(nNPVBins, vector<double>(nPtBins, 0)));
  errAvgResponse = vector<vector<vector<double> > >(fitNames.size(), vector<vector<double> >(nNPVBins, vector<double>(nPtBins, 0)));
  avgPtTrue = vector<vector<vector<double> > >(fitNames.size(), vector<vector<double> >(nNPVBins, vector<double>(nPtBins, 0)));
  errAvgPtTrue = vector<vector<vector<double> > >(fitNames.size(), vector<vector<double> >(nNPVBins, vector<double>(nPtBins, 0)));
  avgNonIsoResponse = vector<vector<vector<double> > >(fitNames.size(), vector<vector<double> >(nNPVBins, vector<double>(nPtBins, 0)));
  errAvgNonIsoResponse = vector<vector<vector<double> > >(fitNames.size(), vector<vector<double> >(nNPVBins, vector<double>(nPtBins, 0)));

  vector<vector<vector<double> > > resolutionFit(nFitNames, vector<vector<double> >(nNPVBins, vector<double>(nPtBins,0)));
  vector<vector<vector<double> > > relResolutionFit(nFitNames, vector<vector<double> >(nNPVBins, vector<double>(nPtBins,0)));
  vector<vector<vector<double> > > errResolutionFit(nFitNames, vector<vector<double> >(nNPVBins, vector<double>(nPtBins,0)));
  vector<vector<vector<double> > > errRelResolutionFit(nFitNames, vector<vector<double> >(nNPVBins, vector<double>(nPtBins,0)));

  vector<vector<double> > responseInclusive(nFitNames, vector<double>(nPtBins,0));
  vector<vector<double> > resolutionFitInclusive(nFitNames, vector<double>(nPtBins,0));
  vector<vector<double> > relResolutionFitInclusive(nFitNames, vector<double>(nPtBins,0));
  vector<vector<double> > errResolutionFitInclusive(nFitNames, vector<double>(nPtBins,0));
  vector<vector<double> > errRelResolutionFitInclusive(nFitNames, vector<double>(nPtBins,0));
  vector<vector<double> > errResponseInclusive(nFitNames, vector<double>(nPtBins,0));

  vector<vector<vector<double> > > responseAIQR(nFitNames, vector<vector<double> >(nNPVBins, vector<double>(nPtBins,0)));
  vector<vector<vector<double> > > resolutionAIQR(nFitNames, vector<vector<double> >(nNPVBins, vector<double>(nPtBins,0)));
  vector<vector<vector<double> > > relResolutionAIQR(nFitNames, vector<vector<double> >(nNPVBins, vector<double>(nPtBins,0)));
  vector<vector<vector<double> > > errResponseAIQR(nFitNames, vector<vector<double> >(nNPVBins, vector<double>(nPtBins,0)));
  vector<vector<vector<double> > > errResolutionAIQR(nFitNames, vector<vector<double> >(nNPVBins, vector<double>(nPtBins,0)));
  vector<vector<vector<double> > > errRelResolutionAIQR(nFitNames, vector<vector<double> >(nNPVBins, vector<double>(nPtBins,0)));

  vector<vector<double> > responseAIQRInclusive(nFitNames, vector<double>(nPtBins,0));
  vector<vector<double> > resolutionAIQRInclusive(nFitNames, vector<double>(nPtBins,0));
  vector<vector<double> > relResolutionAIQRInclusive(nFitNames, vector<double>(nPtBins,0));
  vector<vector<double> > errResponseAIQRInclusive(nFitNames, vector<double>(nPtBins,0));
  vector<vector<double> > errResolutionAIQRInclusive(nFitNames, vector<double>(nPtBins,0));
  vector<vector<double> > errRelResolutionAIQRInclusive(nFitNames, vector<double>(nPtBins,0));

  vector<vector<double> > NonIsoResponseInclusive(nFitNames, vector<double>(nPtBins,0));
  vector<vector<double> > errNonIsoResponseInclusive(nFitNames, vector<double>(nPtBins,0));
	c1->cd();
	c1->Print(Form("%sResponseFits.pdf[", calibType.c_str()));
	c1->Print(Form("Quantiles%sResponseFits.pdf[", calibType.c_str()));
  c1->Print(Form("%sResponseInclusiveFits.pdf[", calibType.c_str()));
  c1->Print(Form("Quantiles%sResponseInclusiveFits.pdf[", calibType.c_str()));
  c1->Print(Form("%sIQRResponseInclusiveFits.pdf[", calibType.c_str()));
	c1->Print(Form("%sIQRResponseFits.pdf[", calibType.c_str()));
	c1->Print(Form("%sPtRecoFits.pdf[", calibType.c_str()));

  for(unsigned int ptbin=0; ptbin<nPtBins; ptbin++){
    for(unsigned int fit=0; fit<fitNames.size(); fit++){
      double ptSpacing =  (maxPt - minPt )*1.0 / nPtBins;
      double lowPt = minPt + ptSpacing * ptbin;
      double highPt = lowPt + ptSpacing;


      double totalweightinclusive =   truthAllWeightInclusive[fit][ptbin];
			TH1F *h_responseInclusive = new TH1F(Form("h_%sResponseAllInclusive%s_Pt_%0.f_%.0f",calibType.c_str(),fitNames[fit].c_str(), lowPt, highPt), Form(";%s;", responseString.c_str()), nResponseBins, minResponse, maxResponse);
      TH1F *h_nonIsoResponseInclusive = new TH1F(Form("h_%sNonIsoResponseAllInclusive%s_Pt_%0.f_%.0f",calibType.c_str(),fitNames[fit].c_str(), lowPt, highPt), Form(";%s;", responseString.c_str()), nResponseBins, minResponse, maxResponse);

			vector<double> responseInclusiveNPV, weightInclusiveNPV;
			vector<double> nonIsoResponseInclusiveNPV, nonIsoWeightInclusiveNPV;
		
      for(unsigned int npvbin=0; npvbin<nNPVBins; npvbin++){
      	double npvSpacing =  (maxNPV - minNPV )*1.0 / nNPVBins;
      	double lowNPV = minNPV + npvSpacing * npvbin;
      	double highNPV = lowNPV + npvSpacing;

        double totalWeight = truthAllWeight[fit][npvbin][ptbin];
				double isolationPtCutUncalib;
				if(isCalib == true) isolationPtCutUncalib  = g1(isoPtCut, RecoFits[fit][npvbin]);

				TH1F *h_responseAll = new TH1F(Form("h_%sResponseAll%s_NPV_%.0f_%.0f_Pt_%0.f_%.0f", calibType.c_str(), fitNames[fit].c_str(), lowNPV, highNPV, lowPt, highPt), Form(";%s;", responseString.c_str()), nResponseBins, minResponse, maxResponse);
				TH1F *h_nonIsoResponseAll = new TH1F(Form("h_%sNonIsoResponseAll%s_NPV_%.0f_%.0f_Pt_%0.f_%.0f",calibType.c_str(), fitNames[fit].c_str(), lowNPV, highNPV, lowPt, highPt), Form(";%s;", responseString.c_str()), nResponseBins, minResponse, maxResponse);

				vector<double> response, weights, pttrue, nonIsoResponse, nonIsoWeights, nonIsoPtTrue;

        for(unsigned int j=0; j<dataPt[fit][npvbin][ptbin].size(); j++){
					bool isIsolated = dataIsolation[fit][npvbin][ptbin][j];
					double ptTrue = dataPtTrue[fit][npvbin][ptbin][j];
					double weight = dataWeight[fit][npvbin][ptbin][j];
					double ptReco = dataPt[fit][npvbin][ptbin][j];
					
					if(isCalib == true) 	ptReco = g1(ptReco, RecoFits[fit][npvbin]);

					if(isIsolated == true){
						response.push_back(ptReco / ptTrue);
						responseInclusiveNPV.push_back(ptReco/ptTrue);
          	h_responseAll->Fill(ptReco / ptTrue, weight);
          	h_responseInclusive->Fill(ptReco / ptTrue, weight);
						weights.push_back(weight);
						weightInclusiveNPV.push_back(weight);
						pttrue.push_back(ptTrue);
					}

          nonIsoResponse.push_back(ptReco / ptTrue);
          nonIsoResponseInclusiveNPV.push_back(ptReco/ptTrue);
          nonIsoWeights.push_back(weight);
          nonIsoWeightInclusiveNPV.push_back(weight);
          nonIsoPtTrue.push_back(ptTrue);
          h_nonIsoResponseAll->Fill(ptReco / ptTrue, weight);
          h_nonIsoResponseInclusive->Fill(ptReco / ptTrue, weight);
        }


        //Fitted Gaussian Method
        TF1 *gfit = getFit(Form("%sResponseFits.pdf",calibType.c_str()), h_responseAll, nSigma, nSigma, fit, ptbin, npvbin);
        avgResponse[fit][npvbin][ptbin] = gfit->GetParameter(1);
        resolutionFit[fit][npvbin][ptbin] = gfit->GetParameter(2);
        errAvgResponse[fit][npvbin][ptbin] = gfit->GetParError(1);
        relResolutionFit[fit][npvbin][ptbin] = gfit->GetParameter(2) / gfit->GetParameter(1);
        errResolutionFit[fit][npvbin][ptbin] = gfit->GetParError(2);
      	double resFitErr = gfit->GetParError(2);
      	double respFitErr = gfit->GetParError(1);
      	double respFit = gfit->GetParameter(1);
      	double resFit = gfit->GetParameter(2);
      	// http://lectureonline.cl.msu.edu/~mmp/labs/error/e2.htm
				double errRelRes = (resFit / respFit) * sqrt( (respFitErr*respFitErr)/(respFit*respFit) + (respFitErr*respFitErr)/(respFit*respFit));
        errRelResolutionFit[fit][npvbin][ptbin] = errRelRes;
        h_responseAll->Scale(1./h_responseAll->Integral(), "width");

				avgPtTrue[fit][npvbin][ptbin] = average(pttrue, weights);





        //IQR
        double leftiqr = absolutelhsiqr(nonIsoResponse, nonIsoWeights, totalWeight, 0.158);
        double rightiqr = absolutelhsiqr(nonIsoResponse, nonIsoWeights, totalWeight, 0.842);
        double medianiqr = absolutelhsiqr(nonIsoResponse, nonIsoWeights, totalWeight, 0.5);
        double rightaiqr = absoluterhsiqr(nonIsoResponse, nonIsoWeights, totalWeight, 0.3413, avgResponse[fit][npvbin][ptbin]);
        resolutionAIQR[fit][npvbin][ptbin] = rightaiqr;
        relResolutionAIQR[fit][npvbin][ptbin] = rightaiqr / avgResponse[fit][npvbin][ptbin];
        double efficiency = absoluteefficiency(nonIsoResponse, nonIsoWeights, totalWeight);
        drawIQR(h_nonIsoResponseAll, leftiqr, medianiqr, rightiqr, efficiency, ptbin, npvbin, fit);
        c1->cd();
        c1->Print(Form("%sIQRResponseFits.pdf", calibType.c_str()));
        double varianceNonIso = getVariance(nonIsoResponse, nonIsoWeights, medianiqr);
        double sumWeightsSqNonIso = getWeightSq(nonIsoResponse, nonIsoWeights, medianiqr);
        //double std_err_NonIso = varianceNonIso / sqrt(sumWeightsSqNonIso);
        double std_err_NonIso = sqrt(varianceNonIso/sumWeightsSqNonIso);
        errResolutionAIQR[fit][npvbin][ptbin] = 1.573*std_err_NonIso;
        errResponseAIQR[fit][npvbin][ptbin] = 1.2533*sqrt(varianceNonIso/sumWeightsSqNonIso);
        double resAIQRErr =  errResponseAIQR[fit][npvbin][ptbin];
        double respAIQRErr = errResolutionAIQR[fit][npvbin][ptbin];
        double respAIQR = medianiqr;
        double resAIQR = resolutionAIQR[fit][npvbin][ptbin];
        // http://lectureonline.cl.msu.edu/~mmp/labs/error/e2.htm
        errRelResolutionAIQR[fit][npvbin][ptbin] = (resAIQR / respAIQR) * sqrt( (respAIQRErr*respAIQRErr)/(respAIQR*respAIQR) + (respAIQRErr*respAIQRErr)/(respAIQR*respAIQR));

      }

			f->cd();
			h_responseInclusive->Write();


			//Fitted Gaussian
      TF1 *gfit = getFit(Form("%sResponseInclusiveFits.pdf",calibType.c_str()),h_responseInclusive, nSigma, nSigma, fit,ptbin, -1);
      responseInclusive[fit][ptbin] = gfit->GetParameter(1);
      errResponseInclusive[fit][ptbin] = gfit->GetParError(1);
      resolutionFitInclusive[fit][ptbin] = gfit->GetParameter(2);
      relResolutionFitInclusive[fit][ptbin] = gfit->GetParameter(2) / gfit->GetParameter(1);
      errResolutionFitInclusive[fit][ptbin] = gfit->GetParError(2);
			double resFitErr = gfit->GetParError(2);
			double respFitErr = gfit->GetParError(1);
			double respFit = gfit->GetParameter(1);
			double resFit = gfit->GetParameter(2);
			// http://lectureonline.cl.msu.edu/~mmp/labs/error/e2.htm
      errRelResolutionFitInclusive[fit][ptbin] = (resFit / respFit) * sqrt( (respFitErr*respFitErr)/(respFit*respFit) + (respFitErr*respFitErr)/(respFit*respFit));
      h_responseInclusive->Scale(1./h_responseInclusive->Integral(), "width");



      //IQR
      double leftiqr = absolutelhsiqr(nonIsoResponseInclusiveNPV, nonIsoWeightInclusiveNPV, totalweightinclusive, 0.158);
      double rightiqr = absolutelhsiqr(nonIsoResponseInclusiveNPV, nonIsoWeightInclusiveNPV, totalweightinclusive, 0.842);
      double medianiqr = absolutelhsiqr(nonIsoResponseInclusiveNPV, nonIsoWeightInclusiveNPV, totalweightinclusive, 0.5);
      double rightaiqr = absoluterhsiqr(nonIsoResponseInclusiveNPV, nonIsoWeightInclusiveNPV, totalweightinclusive, 0.3413, responseInclusive[fit][ptbin]);
      double varianceNonIso = getVariance(nonIsoResponseInclusiveNPV, nonIsoWeightInclusiveNPV, medianiqr);
      double sumWeightsSqInclusiveNonIso = getWeightSq(nonIsoResponseInclusiveNPV, nonIsoWeightInclusiveNPV, medianiqr);
      double std_err_NonIso = varianceNonIso / sqrt(sumWeightsSqInclusiveNonIso);

      double errResponseAIQR = 1.2533*sqrt(varianceNonIso/sumWeightsSqInclusiveNonIso);
      resolutionAIQRInclusive[fit][ptbin] = rightaiqr;
      relResolutionAIQRInclusive[fit][ptbin] = rightaiqr/responseInclusive[fit][ptbin];
      double efficiency = absoluteefficiency(nonIsoResponseInclusiveNPV, nonIsoWeightInclusiveNPV, totalweightinclusive);

      drawIQR(h_nonIsoResponseInclusive, leftiqr, medianiqr, rightiqr, efficiency, ptbin, -1, fit);
      c1->cd();
      c1->Print(Form("%sIQRResponseInclusiveFits.pdf", calibType.c_str()));
      double resAIQRErr =  1.573*std_err_NonIso;
      double respAIQRErr = 1.2533*sqrt(varianceNonIso/sumWeightsSqInclusiveNonIso);
      double respAIQR = medianiqr;
      double resAIQR = resolutionAIQRInclusive[fit][ptbin];
      // http://lectureonline.cl.msu.edu/~mmp/labs/error/e2.htm
      errRelResolutionAIQRInclusive[fit][ptbin] = (resAIQR / respAIQR) * sqrt( (respAIQRErr*respAIQRErr)/(respAIQR*respAIQR) + (respAIQRErr*respAIQRErr)/(respAIQR*respAIQR));
      errResolutionAIQRInclusive[fit][ptbin] = 1.573*std_err_NonIso;
      h_nonIsoResponseInclusive->Scale(1./h_nonIsoResponseInclusive->Integral(), "width");
    }
  }
	c1->cd();
  c1->Print(Form("%sResponseFits.pdf]", calibType.c_str()));
  c1->Print(Form("Quantiles%sResponseFits.pdf]", calibType.c_str()));
  c1->Print(Form("%sResponseInclusiveFits.pdf]", calibType.c_str()));
  c1->Print(Form("Quantiles%sResponseInclusiveFits.pdf]", calibType.c_str()));
  c1->Print(Form("%sPtRecoFits.pdf]", calibType.c_str()));

  printHists(Form("%sResolutionAIQR", calibType.c_str()),  resolutionAIQR, aiqrString, errResolutionAIQR);
  printHists(Form("%sRelativeResolutionAIQR", calibType.c_str()),  relResolutionAIQR, aiqrString, errRelResolutionAIQR);
  printHists(Form("%sResolutionAIQRInclusive", calibType.c_str()), resolutionAIQRInclusive, "pt", aiqrString, errResolutionAIQRInclusive);
  printHists(Form("%sRelativeResolutionAIQRInclusive", calibType.c_str()), relResolutionAIQRInclusive, "pt", aiqrString, errRelResolutionAIQRInclusive);
  printRatio(Form("%sResolutionAIQR", calibType.c_str()),  resolutionAIQR, aiqrString, errResolutionAIQR, 0);
  printRatio(Form("%sRelativeResolutionAIQR", calibType.c_str()),  relResolutionAIQR, aiqrString, errRelResolutionAIQR,0);
  printRatio(Form("%sResolutionAIQRInclusive", calibType.c_str()), resolutionAIQRInclusive, "pt", aiqrString, errResolutionAIQRInclusive,0);
  printRatio(Form("%sRelativeResolutionAIQRInclusive", calibType.c_str()), relResolutionAIQRInclusive, "pt", aiqrString, errRelResolutionAIQRInclusive,0);

  printHists(Form("%sResponse", calibType.c_str()),  avgResponse, responseString, errAvgResponse);
  printHists(Form("%sResponseInclusive", calibType.c_str()), responseInclusive, "pt", responseString, errResponseInclusive);
  printHists(Form("%sResolutionFit", calibType.c_str()), resolutionFit, resolutionString, errResolutionFit);
  printHists(Form("%sRelativeResolutionFit", calibType.c_str()), relResolutionFit, resolutionString, errRelResolutionFit);
  printHists(Form("%sResolutionFitInclusive", calibType.c_str()), resolutionFitInclusive, "pt", resolutionString, errResolutionFitInclusive);
  printHists(Form("%sRelativeResolutionFitInclusive", calibType.c_str()), relResolutionFitInclusive, "pt", resolutionString, errRelResolutionFitInclusive);
  printRatio(Form("%sResolutionFit", calibType.c_str()), resolutionFit, resolutionString, errResolutionFit, 0);
  printRatio(Form("%sRelativeResolutionFit", calibType.c_str()), relResolutionFit, resolutionString, errRelResolutionFit, 0);
  printRatio(Form("%sRelativeResolutionFitInclusive", calibType.c_str()), relResolutionFitInclusive, "pt", resolutionString, errRelResolutionFitInclusive,0);
}

// Gets the RHS variance for Standard IQR
double calibration::getRHSVariance(vector<double> data, vector<double> weights, double mean){
  double sum = 0;
  double sumWeights = 0;
  for(unsigned int i=0; i<data.size(); i++){
    if(data[i] < mean) continue;
    sum += weights[i]*(data[i] - mean)*(data[i]-mean);
    sumWeights += weights[i];
  }

  return sum / sumWeights;
}

// Gets the RHS variance for Standard IQR
double calibration::getVariance(vector<double> data, vector<double> weights, double median){
  double sum = 0;
  double sumWeights = 0;
  for(unsigned int i=0; i<data.size(); i++){
    sum += weights[i]*(data[i] - median)*(data[i]-median);
    sumWeights += weights[i];
  }

  return sum / sumWeights;
}


double calibration::getRHSWeightSq(vector<double> data, vector<double> weights, double mean){
  double sumWeightsSq = 0;
  for(unsigned int i=0; i<data.size(); i++){
    if(data[i] < mean) continue;
    sumWeightsSq += weights[i]*weights[i];
  }

  return sumWeightsSq;
}

double calibration::getWeightSq(vector<double> data, vector<double> weights, double mean){
  double sumWeightsSq = 0;
  for(unsigned int i=0; i<data.size(); i++){
    sumWeightsSq += weights[i]*weights[i];
  }

  return sumWeightsSq;
}

void calibration::makeCalibrationCurves(){
	c1->cd();
	c1->Print("calibrationCurves.pdf[");
  for(unsigned int npvbin=0; npvbin<nNPVBins; npvbin++){
    for(unsigned int fit=0; fit<fitNames.size(); fit++){
      //Fit to response vs. pTtrue
			TH1F *h_resp = new TH1F(Form("h_resp%d%d",fit, npvbin), Form(";p_{t}^{true};%s", responseString.c_str()), nPtBins, minPt, maxPt);
			for(unsigned int ptbin=0; ptbin < nPtBins; ptbin++){ 
				h_resp->SetBinContent(ptbin+1, avgResponse[fit][npvbin][ptbin]);
				h_resp->SetBinError(ptbin+1, errAvgResponse[fit][npvbin][ptbin]);
			}

      TGraph *responseCurve = new TGraph(nPtBins, &(avgPtTrue[fit][npvbin][0]), &(avgResponse[fit][npvbin][0]));
      TF1 *RespFunction = new TF1(Form("RespFunction%d%d", fit, npvbin), "[0] + [1]/log(x + 10) + [2]/(pow(log(x + 10),2))", 0, 200);
      TF1 *RecoFunction = new TF1(Form("RecoFunction%d%d", fit, npvbin), "x*[0] + x*[1]/log(x + 10) + x*[2]/(pow(log(x+10),2))", 0, 200);

      responseCurve->Fit(RespFunction, "RQ");
			h_resp->UseCurrentStyle();
			h_resp->SetMarkerColor( TColor::GetColor(colors[fit]) );
      RecoFunction->SetParameters(RespFunction->GetParameter(0), RespFunction->GetParameter(1), RespFunction->GetParameter(2), RespFunction->GetParameter(3));
			h_resp->Draw("Ex0C hist");
			f->cd();
			h_resp->Write();

			RespFunction->Draw("SAME");
			h_resp->Draw("P SAME");
      RecoFits[fit][npvbin] = RecoFunction;
      makeLegends(true);
      makeLabels("npv", npvbin, true);
			c1->Print("calibrationCurves.pdf");
    }
  }
	c1->Print("calibrationCurves.pdf]");

  ofstream myfile;
	myfile.open ("recoFits.txt");


	for(unsigned int i=0; i<fitNames.size(); i++){
		for(unsigned int j=0; j<nNPVBins; j++){
			myfile << fitNames[i] << "\t"  << j << "\t" << nNPVBins << "\t" << RecoFits[i][j]->GetParameter(0) << "\t" << RecoFits[i][j]->GetParameter(1) << "\t" << RecoFits[i][j]->GetParameter(2) << "\t" << RecoFits[i][j]->GetParameter(3) <<  "\n";	
		}
	}
	myfile.close();

}

// Returns the weighted average
double calibration::average(vector<double> data, vector<double> weights){
  double sum=0;
  double weightSum = 0;
  for(unsigned int i=0; i<data.size(); i++){
    sum += data[i]*weights[i];
    weightSum += weights[i];
  }
  return (sum / weightSum);
}


void calibration::doFakes(){
	vector<vector<TH1F*> > h_fakeCalibPt = makeHistArray("fakeCalib", "p_{t}^{calib} [GeV]", "Avg. Number of Fake Jets", nPtBins, minPt, maxPt, nNPVBins, "NPV");
	vector<TH1F*> h_nFakeJets = makeHists("nFakeJets", "NPV", "Avg. Number of Fake Jets", nNPVBins, minNPV, maxNPV, fitNames.size(), -1,"fits");
	vector<vector<TH1F*> > h_allCalibPt = makeHistArray("allCalib", "p_{t}^{calib} [GeV]", "Avg. Number of Jets", nPtBins, minPt, maxPt, nNPVBins, "NPV");
	vector<TH1F*> h_nAllJets = makeHists("nAllJets", "NPV", "Avg. Number of Jets", nNPVBins, minNPV, maxNPV, fitNames.size(), -1, "fits");
	vector<TH1F*> h_nTruthJets = makeHists("nTruthJets", "NPV", "Avg. Number of Jets", nNPVBins, minNPV, maxNPV, fitNames.size(), -1, "fits");

  for(unsigned int npvbin=0; npvbin<nNPVBins; npvbin++){
    for(unsigned int fit=0; fit<fitNames.size(); fit++){
			double ptThresh = RecoFits[fit][npvbin]->Eval(minPt);
			cout << ptThresh << endl;
      for(unsigned int j=0; j<fakeDataPt[fit][npvbin].size(); j++){
				if(fakeDataPt[fit][npvbin][j] < ptThresh) continue;
        double calibpt = g1(fakeDataPt[fit][npvbin][j], RecoFits[fit][npvbin]);
				double weight = fakeDataWeight[fit][npvbin][j];
				h_nFakeJets[fit]->AddBinContent(npvbin+1, weight);
				h_fakeCalibPt[fit][npvbin]->Fill(calibpt, weight);
      }

      for(unsigned int j=0; j<allDataPt[fit][npvbin].size(); j++){
        if(allDataPt[fit][npvbin][j] < ptThresh) continue;
        double calibpt = g1(allDataPt[fit][npvbin][j], RecoFits[fit][npvbin]);
        double weight = allDataWeight[fit][npvbin][j];
        h_nAllJets[fit]->AddBinContent(npvbin+1, weight);
        h_allCalibPt[fit][npvbin]->Fill(calibpt, weight);
      }

      for(unsigned int j=0; j<truthDataWeight[fit][npvbin].size(); j++){
        double weight = truthDataWeight[fit][npvbin][j];
        h_nTruthJets[fit]->AddBinContent(npvbin+1, weight);
      }

    }
  }

	//Need to scale by weighted number of events instead of typical normalization
  for(unsigned int fit=0; fit<fitNames.size(); fit++){
    for(unsigned int npvbin=0; npvbin<nNPVBins; npvbin++){
      double binContent = h_nFakeJets[fit]->GetBinContent(npvbin+1);
      double truthBinContent = h_nTruthJets[fit]->GetBinContent(npvbin+1);
      h_nFakeJets[fit]->SetBinContent(npvbin+1, binContent/sumWeightNPV[npvbin]);
      h_nTruthJets[fit]->SetBinContent(npvbin+1, truthBinContent/sumWeightNPV[npvbin]);
      h_fakeCalibPt[fit][npvbin]->Scale(1./sumWeightNPV[npvbin]);

    }
  }

  //Need to scale by weighted number of events instead of typical normalization
  for(unsigned int fit=0; fit<fitNames.size(); fit++){
    for(unsigned int npvbin=0; npvbin<nNPVBins; npvbin++){
      double binContent = h_nAllJets[fit]->GetBinContent(npvbin+1);
      h_nAllJets[fit]->SetBinContent(npvbin+1, binContent/sumWeightNPV[npvbin]);
      h_allCalibPt[fit][npvbin]->Scale(1./sumWeightNPV[npvbin]);
    }
  }


  drawHists("nJets", h_nAllJets, "npv", false);
  drawHists("nTruthJets", h_nTruthJets, "npv", false);
	drawHists("allCalibPt", h_allCalibPt, "pt", false);
  drawHists("nFakeJets", h_nFakeJets, "npv", false);
  drawHists("fakeCalibPt", h_fakeCalibPt, "pt", false);

}

void calibration::finalize(){
	f->cd();
	f->Close();
}


void calibration::doEfficiency(){
	vector<vector<double> >	truthMatchedRatioInclusive(fitNames.size(),  vector<double>(nPtBins, 0));
  for(unsigned int fit=0; fit<fitNames.size(); fit++){
    for(unsigned int ptbin=0; ptbin<nPtBins; ptbin++){
			double sumTrueIncl = 0;
			double sumTruthAllWeight = 0;
			

  		for(unsigned int npvbin=0; npvbin<nNPVBins; npvbin++){
    		double ptThresh = RecoFits[fit][npvbin]->Eval(efficiencyCalibPtCut);
				double sumTrue = 0;
				for(unsigned int j=0; j<truthMatchedReco[fit][npvbin][ptbin].size(); j++){
					if(truthMatchedReco[fit][npvbin][ptbin][j] >= ptThresh){
						sumTrue += truthMatchedWeight[fit][npvbin][ptbin][j];
						sumTrueIncl += truthMatchedWeight[fit][npvbin][ptbin][j];
					}
				}
				sumTruthAllWeight += truthAllWeight[fit][npvbin][ptbin];
    		truthMatchedRatio[fit][npvbin][ptbin] = sumTrue / truthAllWeight[fit][npvbin][ptbin];
      }
    	truthMatchedRatioInclusive[fit][ptbin] = sumTrueIncl / sumTruthAllWeight;
    }
  }
	vector<vector<vector<double> > > noErr;
	vector<vector<double> > noErrInclusive;
  printHists("efficiency", truthMatchedRatio, "Efficiency", noErr);
  printHists("EfficiencyInclusive", truthMatchedRatioInclusive, "pt", "Efficiency", noErrInclusive);
//  printHists(Form("%sResolutionFitInclusive", calibType.c_str()), resolutionFitInclusive, "pt", resolutionString, errResolutionFitInclusive);
}

void calibration::printRatio(string name, vector<vector<vector<double> > > data, string yAxis, vector<vector<vector<double> > > err, unsigned int fitRat){
  vector<vector<TH1F*> > histsPt = makeHistArray(Form("%sRatio",name.c_str()), "p_{t}^{true} [GeV]", yAxis, nPtBins, minPt, maxPt, nNPVBins, "NPV");
  vector<vector<TH1F*> > histsNPV = makeHistArray(Form("%sRatio", name.c_str()), "NPV", yAxis, nNPVBins, minNPV, maxNPV, nPtBins, "Pt");
  vector<vector<TH1F*> > ratioHistsPt = makeHistArray(Form("%sRatioTest",name.c_str()), "p_{t}^{true} [GeV]", "Ratio to Area Sub", nPtBins, minPt, maxPt, nNPVBins, "NPV");
  vector<vector<TH1F*> > ratioHistsNPV = makeHistArray(Form("%sRatioTest", name.c_str()), "NPV", "Ratio to Area Sub", nNPVBins, minNPV, maxNPV, nPtBins, "Pt");

  for(unsigned int ptbin=0; ptbin < nPtBins; ptbin++){
    for(unsigned int npvbin=0; npvbin<nNPVBins; npvbin++){
    	for(unsigned int fit=0; fit<fitNames.size(); fit++){
        histsPt[fit][npvbin]->SetBinContent(ptbin+1, data[fit][npvbin][ptbin]);
        histsNPV[fit][ptbin]->SetBinContent(npvbin+1, data[fit][npvbin][ptbin]);
        if(err.size() > 0){
          histsPt[fit][npvbin]->SetBinError(ptbin+1, err[fit][npvbin][ptbin]);
          histsNPV[fit][ptbin]->SetBinError(npvbin+1, err[fit][npvbin][ptbin]);
        }

				if(fitRat == fit){
					for(unsigned int fit2 = 0; fit2 < fitNames.size(); fit2++){
      	  	ratioHistsPt[fit2][npvbin]->SetBinContent(ptbin+1, data[fit][npvbin][ptbin]);
    	    	ratioHistsNPV[fit2][ptbin]->SetBinContent(npvbin+1, data[fit][npvbin][ptbin]);
  	      	if(err.size() > 0){
	          	ratioHistsPt[fit2][npvbin]->SetBinError(ptbin+1, err[fit][npvbin][ptbin]);
          		ratioHistsNPV[fit2][ptbin]->SetBinError(npvbin+1, err[fit][npvbin][ptbin]);
        		}
					}
				}

      }
    }
  }


  bool hasErr = false;
  if(err.size() > 0) hasErr = true;
  drawRatio( Form("%sPtRatio", name.c_str()), histsPt, ratioHistsPt,  "npv", hasErr);
  drawRatio( Form("%sNPVRatio", name.c_str()), histsNPV, ratioHistsNPV, "pt", hasErr);
}



void calibration::printHists(string name, vector<vector<vector<double> > > data, string yAxis, vector<vector<vector<double> > > err){
  vector<vector<TH1F*> > histsPt = makeHistArray(Form("%s",name.c_str()), "p_{t}^{true} [GeV]", yAxis, nPtBins, minPt, maxPt, nNPVBins, "NPV");
  vector<vector<TH1F*> > histsNPV = makeHistArray(Form("%s", name.c_str()), "NPV", yAxis, nNPVBins, minNPV, maxNPV, nPtBins, "Pt");

  for(unsigned int ptbin=0; ptbin < nPtBins; ptbin++){
    for(unsigned int fit=0; fit<fitNames.size(); fit++){
      for(unsigned int npvbin=0; npvbin<nNPVBins; npvbin++){
        histsPt[fit][npvbin]->SetBinContent(ptbin+1, data[fit][npvbin][ptbin]);
        histsNPV[fit][ptbin]->SetBinContent(npvbin+1, data[fit][npvbin][ptbin]);
        if(err.size() > 0){
          histsPt[fit][npvbin]->SetBinError(ptbin+1, err[fit][npvbin][ptbin]);
          histsNPV[fit][ptbin]->SetBinError(npvbin+1, err[fit][npvbin][ptbin]);
        }
      }
    }
  }

	bool hasErr = false;
	if(err.size() > 0) hasErr = true;
  drawHists( Form("%sPt", name.c_str()), histsPt,  "npv", hasErr);
  drawHists( Form("%sNPV", name.c_str()), histsNPV, "pt", hasErr);
}



void calibration::printHists(string name, vector<vector<double> > data, string type, string yAxis, vector<vector<double> > err){
  vector<TH1F*> hists = makeHists(Form("%s",name.c_str()), "p_{t}^{true} [GeV]", yAxis, nPtBins, minPt, maxPt, fitNames.size(), -1, "fits");

  for(unsigned int fit=0; fit<fitNames.size(); fit++){
    for(unsigned int bin=0; bin < data[fit].size(); bin++){
      hists[fit]->SetBinContent(bin+1, data[fit][bin]);
      if(err.size() > 0) hists[fit]->SetBinError(bin+1, err[fit][bin]);
    }
  }

  bool hasErr = false;
  if(err.size() > 0) hasErr = true;
  drawHists(Form("%s%s", name.c_str(), type.c_str()), hists, type, hasErr);
}


void calibration::printRatio(string name, vector<vector<double> > data, string type, string yAxis, vector<vector<double> > err, unsigned int fitRat){
  vector<TH1F*> hists = makeHists(Form("%sRatio",name.c_str()), "p_{t}^{true} [GeV]", yAxis, nPtBins, minPt, maxPt, fitNames.size(), -1, "fits");
  vector<TH1F*> ratHists = makeHists(Form("%sRatioTest",name.c_str()), "p_{t}^{true} [GeV]", yAxis, nPtBins, minPt, maxPt, fitNames.size(), -1, "fits");

  for(unsigned int fit=0; fit<fitNames.size(); fit++){
    for(unsigned int bin=0; bin < data[fit].size(); bin++){
      hists[fit]->SetBinContent(bin+1, data[fit][bin]);
      if(err.size() > 0) hists[fit]->SetBinError(bin+1, err[fit][bin]);
		if(fit == fitRat){
			for(unsigned int fit2=0; fit2<fitNames.size(); fit2++){
      	ratHists[fit2]->SetBinContent(bin+1, data[fit][bin]);
      	if(err.size() > 0) ratHists[fit2]->SetBinError(bin+1, err[fit][bin]);
			}
		}
    }
  }

  bool hasErr = false;
  if(err.size() > 0) hasErr = true;
  drawRatio(Form("%s%sRatio", name.c_str(), type.c_str()), hists, ratHists, type, hasErr);
}


void calibration::makeLabels(bool doSmall){
	if(doSmall == false){
 		gStyle->SetTextSize(0.06);
    ATLASLabel(.15, .88, "Simulation Internal",1);
    myText( 0.15, 0.82, 1, "Pythia Dijet #sqrt{s}= 13 TeV");
    if(isEM == 0)myText( 0.15, 0.76, 1, "anti-k_{t} LCW, R=0.4");
    else myText( 0.15, 0.76, 1, "anti-k_{t} EM, R=0.4");
		gStyle->SetTextSize(0.04);
	}

	if(doSmall == true){
		gStyle->SetTextSize(0.04);
    ATLASLabel(.2, .86, "Simulation Internal",1);
    myText( 0.2, 0.82, 1, "Pythia Dijet #sqrt{s}= 13 TeV");
    if(isEM == 0)myText( 0.2, 0.78, 1, "anti-k_{t} LCW, R=0.4");
    else myText( 0.2, 0.78, 1, "anti-k_{t} EM, R=0.4");
	}
}

void calibration::makeLabels(string datatype, int databin, bool doSmall){
		makeLabels(doSmall);
		if(doSmall == false) gStyle->SetTextSize(0.06);


		if(strcmp("npv", datatype.c_str())==0){
    	double deltaNPV = (maxNPV - minNPV) / nNPVBins;
    	double lowNPV = minNPV + databin*deltaNPV;
    	double highNPV = lowNPV + deltaNPV;
			if(doSmall == true)	myText( 0.2, 0.74, 1, Form("%2.0f < NPV < %2.0f", lowNPV, highNPV));
			if(doSmall == false)	myText( 0.15, 0.70, 1, Form("%2.0f < NPV < %2.0f", lowNPV, highNPV));
		}

		else{
      double deltaPt = (maxPt - minPt) / nPtBins;
      double lowPt = minPt + databin*deltaPt;
      double highPt = lowPt + deltaPt;
      if(doSmall == true) myText( 0.2, 0.74, 1, Form("%2.0f < Pt < %2.0f", lowPt, highPt));
      if(doSmall == false) myText( 0.15, 0.70, 1, Form("%2.0f < Pt < %2.0f", lowPt, highPt));
    }
		if(doSmall == false) gStyle->SetTextSize(0.04);
}

void calibration::makeLabels(int npvbin, int ptbin, bool doSmall){
		makeLabels(doSmall);
		if(doSmall == false) gStyle->SetTextSize(0.06);
    
    double deltaNPV = (1.0*(maxNPV - minNPV)) / nNPVBins;
    double lowNPV = minNPV + npvbin*deltaNPV;
    double highNPV = lowNPV + deltaNPV;

    double deltaPt = (1.0*(maxPt - minPt)) / nPtBins;
    double lowPt = minPt + ptbin*deltaPt;
    double highPt = lowPt + deltaPt;

    if(doSmall == true){
			myText( 0.2, 0.74, 1, Form("%2.0f < NPV < %2.0f", lowNPV, highNPV));
    	myText( 0.2, 0.70, 1, Form("%2.0f < Pt < %2.0f", lowPt, highPt));
		}
		else{
      myText( 0.15, 0.70, 1, Form("%2.0f < NPV < %2.0f", lowNPV, highNPV));
      myText( 0.15, 0.62, 1, Form("%2.0f < Pt < %2.0f", lowPt, highPt));
		}
		if(doSmall == false) gStyle->SetTextSize(0.04);
}


void calibration::makeLegends(bool doSmall){
	if(doSmall == false) gStyle->SetTextSize(0.06);
	for(unsigned int fit=0; fit<fitNames.size(); fit++){
    if(doSmall == true){
    	double Y = .92 - fit*0.04;
			myMarkerText( 0.7, Y, TColor::GetColor(colors[fit]), 20, titleNames[fit].c_str(),1.3);
		}
    if(doSmall == false){
    	double Y = .92 - fit*0.06;
			myMarkerText( 0.7, Y, TColor::GetColor(colors[fit]), 20, titleNames[fit].c_str(),1.3);
		}
  }
	if(doSmall == false) gStyle->SetTextSize(0.04);
}

void calibration::fillData(int n, float ptReco, float ptTruth, float npv, float eventWeight, bool isIsolated){
	if(npv < minNPV || ptTruth < minPt) return;       

  double npvBinSpacing = (maxNPV - minNPV) / nNPVBins;
  unsigned int npvBin = floor((npv - minNPV) / npvBinSpacing);

  double ptBinSpacing = (maxPt - minPt) / nPtBins;
  unsigned int ptBin = floor((ptTruth - minPt) / ptBinSpacing);

  if(ptBin >= nPtBins || npvBin >= nNPVBins) return;

	float ptRecoFloat =  ptReco;
	float ptTruthFloat =  ptTruth;

  dataPt[n][npvBin][ptBin].push_back(ptReco);
  dataPtTrue[n][npvBin][ptBin].push_back(ptTruth);
	dataWeight[n][npvBin][ptBin].push_back(eventWeight);
  dataIsolation[n][npvBin][ptBin].push_back(isIsolated);
}

void calibration::fillFakeData(int n, double ptReco, int npv, double eventWeight, bool isMatched){
	double npvBinSpacing = (maxNPV - minNPV) / nNPVBins;
  unsigned int npvBin = floor((npv - minNPV) / npvBinSpacing);

  allDataPt[n][npvBin].push_back(ptReco);
  allDataWeight[n][npvBin].push_back(eventWeight);
	if(isMatched == false){
  	fakeDataPt[n][npvBin].push_back(ptReco);
  	fakeDataWeight[n][npvBin].push_back(eventWeight);
	}
}

void calibration::fillTruthData(int n, double ptReco, double ptTrue, int npv, double eventWeight, bool isMatched, bool isIsolated){
	if(npv < minNPV || ptTrue < minPt) return;
  double npvBinSpacing = (1.0*(maxNPV - minNPV)) / nNPVBins;
  unsigned int npvBin = (int)((npv - minNPV) / npvBinSpacing);

  double ptBinSpacing = (1.0*(maxPt - minPt)) / nPtBins;
  unsigned int ptBin = (int)((ptTrue - minPt) / ptBinSpacing);
  if(ptBin >= nPtBins || npvBin >= nNPVBins) return;
  truthDataWeight[n][npvBin].push_back(eventWeight);

	if(isMatched == true) {
		NonIsoDataPt[n][npvBin][ptBin].push_back(ptReco);
		NonIsoDataPtTrue[n][npvBin][ptBin].push_back(ptTrue);
		NonIsoDataWeight[n][npvBin][ptBin].push_back(eventWeight);
	}

  if(isMatched == true && isIsolated == true){
		truthMatchedReco[n][npvBin][ptBin].push_back(ptReco);
		truthMatchedWeight[n][npvBin][ptBin].push_back(eventWeight);
	}
  if(isIsolated == true) truthAllWeight[n][npvBin][ptBin] += eventWeight;
  if(isIsolated == true) truthAllWeightInclusive[n][ptBin] += eventWeight;

}

void calibration::fillEntryNPV(double npv, double eventWeight, int nTruth){
  double npvBinSpacing = (maxNPV - minNPV) / nNPVBins;
  int npvbin = floor((npv - minNPV) / npvBinSpacing);
	sumWeightNPV[npvbin] += eventWeight;
}


// g inverse (ptTrue vs ptReco)
// I should double check that the input is ptTrue, not ptReco
double calibration::g1(double ptReco, TF1* Resp){
		double inverse = Resp->GetX(ptReco, 0,200);
	return inverse;
}


// Returns the weighted average
double calibration::quantile(vector<double> data, vector<double> weights, double mean, double quantile){
  double weightSum = 0;
  vector< pair<double, double> > m_dataWeight;

  m_dataWeight.clear();
  for(unsigned int i=0; i<data.size(); i++){
    if(data[i] < mean) continue;
    weightSum += weights[i];
    pair<double, double> myPair(data[i], weights[i]);
    m_dataWeight.push_back(myPair);
  }

  sort(m_dataWeight.begin(), m_dataWeight.end());

  double newWeightSum = 0;
  double quantileVal = 0;
  for(unsigned int i=0; i<m_dataWeight.size(); i++){
    newWeightSum += m_dataWeight[i].second;
    if(newWeightSum > (weightSum * quantile) && quantileVal < 0.00001){
      quantileVal = m_dataWeight[i].first;
      continue;
    }
  }

  return quantileVal - mean;
}

// Returns the "absolute" IQR
double calibration::absoluterhsiqr(vector<double> data, vector<double> weights, double totalWeight, double quantile, double fitMean){
  double weightSum = 0;
  vector< pair<double, double> > m_dataWeight;

  m_dataWeight.clear();
  for(unsigned int i=0; i<data.size(); i++){
		if(data[i] < fitMean) continue;
    weightSum += weights[i];
    pair<double, double> myPair(data[i], weights[i]);
    m_dataWeight.push_back(myPair);
  }

  sort(m_dataWeight.begin(), m_dataWeight.end());
  double newWeightSum = 0;

  double firstquantileVal = 0;
  for(unsigned int i=0; i<m_dataWeight.size(); i++){
    newWeightSum += m_dataWeight[i].second;
    if(newWeightSum > (totalWeight * quantile) && firstquantileVal < 0.00001){
      firstquantileVal = m_dataWeight[i].first;
      continue;
    }
  }

  return firstquantileVal - fitMean;
}


// Returns the "absolute" IQR
double calibration::absolutelhsiqr(vector<double> data, vector<double> weights, double totalWeight, double quantile){
  double weightSum = 0;
  vector< pair<double, double> > m_dataWeight;

  m_dataWeight.clear();
  for(unsigned int i=0; i<data.size(); i++){
    weightSum += weights[i];
    pair<double, double> myPair(data[i], weights[i]);
    m_dataWeight.push_back(myPair);
  }

  sort(m_dataWeight.begin(), m_dataWeight.end());
  double newWeightSum = totalWeight - weightSum;

  double firstquantileVal = 0;
  for(unsigned int i=0; i<m_dataWeight.size(); i++){
    newWeightSum += m_dataWeight[i].second;
    if(newWeightSum > (totalWeight * quantile) && firstquantileVal < 0.00001){
      firstquantileVal = m_dataWeight[i].first;
      continue;
    }
  }

  return firstquantileVal;
}

// Returns the "absolute" IQR
double calibration::absoluteefficiency(vector<double> data, vector<double> weights, double totalWeight){
  double weightSum = 0; 
    
  for(unsigned int i=0; i<data.size(); i++){
    weightSum += weights[i];
  }
  
  return weightSum / totalWeight;
}

pair<double,double> calibration::getRange(vector<TH1F*> hists, bool isRatioPlot){
  double maxVal = -100;
  double minVal = 1000;
  for(unsigned int fit=0; fit<fitNames.size(); fit++){
     if(hists[fit]->GetMaximum() > maxVal) maxVal = hists[fit]->GetMaximum();
     if(hists[fit]->GetMinimum() < minVal) minVal = hists[fit]->GetMinimum();
 	}

  double diff = maxVal - minVal;
  maxVal += diff*0.40;
  minVal -= diff*0.10;

	if(minVal > 0.9 && maxVal < 1.1 && isRatioPlot == false) {
		minVal = 0.9;
		maxVal = 1.1;
	}

	pair<double,double> minmax(minVal, maxVal);
	return minmax;
}

vector<pair<double,double> > calibration::getRange(vector<vector<TH1F*> > hists, bool isRatioPlot){
  vector<pair<double,double> > minmax;

  for(unsigned int bin=0; bin<hists[0].size(); bin++){
		vector<TH1F*> tmpHists;
    for(unsigned int fit=0; fit<fitNames.size(); fit++)	tmpHists.push_back(hists[fit][bin]);
		minmax.push_back(getRange(tmpHists, isRatioPlot));
  }

	return minmax;
}


//Returns a vector of (mean, sigma, meanErr, sigmaErr) for the gaussian fit of the histogram
//Can change how far away from mean you want to fit
//Fits multiple times to get the best fit
TF1* calibration::getFit(string filename, TH1* hist, double nSigmaB, double nSigmaA, int fit, int ptbin, int npvbin){
	c1->cd();
	double mean = hist->GetMean();
	double sigma = hist->GetRMS();
	double max = hist->GetMaximum();
	
	double lowestX = 0;
	double highestX = 0;
	for(int i=0; i<hist->GetNbinsX(); i++){
		if(hist->GetBinContent(i+1) > max*fracCutoff)		highestX = hist->GetBinCenter(i+1);
	}

  for(int i=hist->GetNbinsX()-1; i>=0; i--){
    if(hist->GetBinContent(i+1) > max*fracCutoff)		lowestX = hist->GetBinCenter(i+1);
  }

	//TF1 *gfit;
  TF1 *gfit = new TF1("Gaussian","gaus", mean - nSigmaB * sigma, mean + nSigmaA * sigma); // Create the fit function
	gfit->SetParameters(mean, sigma);
  hist->Fit(gfit,"RQ0"); // Fit histogram h"

  for(int nFit = 0; nFit < 2; nFit ++){
		double minRange = mean - nSigmaB * sigma;
		double maxRange = mean + nSigmaA * sigma;
		if(minRange < lowestX) minRange = lowestX;
		if(maxRange > highestX) maxRange = highestX;
    gfit = new TF1("Gaussian","gaus", minRange, maxRange); // Create the fit function
		gfit->SetParLimits(1, minRange, maxRange);
    hist->Fit(gfit,"RQ0"); // Fit histogram h"
    mean=gfit->GetParameter(1);
    sigma=gfit->GetParameter(2);
  }

	hist->SetLineColor( TColor::GetColor(colors[fit]));
	hist->Draw();
	f->cd();
	hist->Write();
	gfit->Draw("SAME");
	if(npvbin >= 0 && ptbin >= 0) makeLabels(npvbin, ptbin, true);
	else if(ptbin >= 0) makeLabels("pt", ptbin, true);
  myText( 0.2, 0.66, 1, Form("#mu: %2.3f, #sigma: %2.3f", mean, sigma));
	c1->Print(filename.c_str());


	double firstQuantile = mean-sigma;
	double thirdQuantile = mean+sigma;
	drawIQR(hist, firstQuantile, mean , thirdQuantile, 100, ptbin, npvbin,fit);
	c1->Print(Form("Quantiles%s", filename.c_str()));
	

  return gfit;
}


void calibration::makeColors(){
  colors = vector<TString>(12);
  colors[0] = "#ffbe3b";
  colors[1] = "#669900";
  colors[2] = "#ee8573";
  colors[3] = "#035857";
  colors[4] = "#990000";
  colors[5] = "#3eada2";
  colors[6] = "#553366";
  colors[7] = "#bcbc93";
  colors[8] = "#990066";
  colors[9] = "#996600";
  colors[10] = "#004466";
  colors[11] = "#9927ed";

}


void calibration::drawHists(string name, vector<TH1F*> hists, string type, bool hasErr){
	pair<double, double> minmax = getRange(hists, false);
	c1->cd();
  for(unsigned int fit=0; fit<fitNames.size(); fit++){
		f->cd();
		hists[fit]->Write();
		TH1F *hist_new=(TH1F*)hists[fit]->Clone();
    hists[fit]->GetYaxis()->SetRangeUser(minmax.first, minmax.second);
		if(hasErr == true){
    	if(fit == 0)hists[fit]->Draw("Ex0C hist");
      else hists[fit]->Draw("Ex0C hist SAME");
			hists[fit]->Draw("Lhist SAME");
		}
		else{
      if(fit == 0)hists[fit]->Draw("LPhist");
      else hists[fit]->Draw("LPhist SAME");
		}
  }

  makeLegends(true);
  makeLabels(true);
	
  c1->Print(Form("%s.pdf", name.c_str()));
}

void calibration::drawHists(string name, vector<vector<TH1F*> > hists, string type, bool hasErr){
	vector<pair<double, double> > minmax = getRange(hists, false);
	c1->cd();
 c1->Print(Form("%s.pdf[", name.c_str()));
 for(unsigned int bin=0; bin < hists[0].size(); bin++){
    for(unsigned int fit=0; fit<fitNames.size(); fit++){
			f->cd();
			hists[fit][bin]->Write();

      hists[fit][bin]->GetYaxis()->SetRangeUser(minmax[bin].first, minmax[bin].second);
			if(hasErr == true){
      	if(fit == 0)hists[fit][bin]->Draw("Ex0C hist");
      	else hists[fit][bin]->Draw("Ex0C hist SAME");
				hists[fit][bin]->Draw("Lhist SAME");
			}
      else{
        if(fit == 0)hists[fit][bin]->Draw("LPhist");
        else hists[fit][bin]->Draw("LPhist SAME");
      }
    }
    makeLegends(true);
    makeLabels(type, bin, true);
    c1->Print(Form("%s.pdf", name.c_str()));
  }
  c1->Print(Form("%s.pdf]", name.c_str()));
}




void calibration::drawRatio(string name, vector<vector<TH1F*> > hists, vector<vector<TH1F*> > ratHists, string type, bool hasErr){
  vector<pair<double, double> > minmax = getRange(hists, false);
 	c2->Print(Form("%s.pdf[", name.c_str()));
	c2->cd();

	vector<vector<TH1F*> > ratioHists;
	ratioHists.resize(fitNames.size());

  for(unsigned int fit=0; fit<fitNames.size(); fit++){	
  	for(unsigned int bin=0; bin < hists[0].size(); bin++){
      TH1F *hClone = (TH1F*)hists[fit][bin]->Clone(Form("%sClone", hists[fit][bin]->GetName()));
      hClone->Divide(ratHists[fit][bin]);
      hClone->SetMarkerColor(TColor::GetColor(colors[fit]));
      hClone->SetLineColor(TColor::GetColor(colors[fit]));
      hClone->GetXaxis()->SetTitleSize(0.12);
      hClone->GetYaxis()->SetTitleSize(0.12);
      hClone->GetYaxis()->SetLabelSize(0.12);
      hClone->GetYaxis()->SetTitleOffset(0.30);
//      hClone->GetXaxis()->SetTitleOffset(0.30);
      hClone->GetXaxis()->SetLabelSize(0.12);
      hClone->GetYaxis()->SetNdivisions(4, 0, 2);
      hClone->GetYaxis()->SetTitle("Ratio to Area Sub.");
			ratioHists[fit].push_back(hClone);
		}
	}

  vector<pair<double, double> > minmaxRatio = getRange(ratioHists, true);

 	for(unsigned int bin=0; bin < hists[0].size(); bin++){
    for(unsigned int fit=0; fit<fitNames.size(); fit++){
			pad1->cd();


      hists[fit][bin]->GetYaxis()->SetRangeUser(minmax[bin].first, minmax[bin].second*1.08);
			hists[fit][bin]->GetXaxis()->SetLabelSize(0);
			hists[fit][bin]->GetYaxis()->SetTitleSize(.05);
			hists[fit][bin]->GetYaxis()->SetLabelSize(.045);
			hists[fit][bin]->GetYaxis()->SetTitleOffset(0.9);
      if(hasErr == true){
        if(fit == 0)hists[fit][bin]->Draw("Ex0C hist");
        else hists[fit][bin]->Draw("Ex0C hist SAME");
        hists[fit][bin]->Draw("LPhist SAME");
      }
      else{
        if(fit == 0)hists[fit][bin]->Draw("LPhist");
        else hists[fit][bin]->Draw("LPhist SAME");
      }
			pad2->cd();   
			ratioHists[fit][bin]->GetYaxis()->SetRangeUser(minmaxRatio[bin].first, minmaxRatio[bin].second);
			if(fit == 0) ratioHists[fit][bin]->Draw("Ex0C hist");
			else ratioHists[fit][bin]->Draw("Ex0C hist SAME");
			ratioHists[fit][bin]->Draw("LPhist SAME");
	
			f->cd();
			ratioHists[fit][bin]->Write();


    }
		pad1->cd();
    makeLegends(false);
    makeLabels(type, bin, false);
    c2->Print(Form("%s.pdf", name.c_str()));
  }
  c1->Print(Form("%s.pdf]", name.c_str()));
}


void calibration::drawRatio(string name, vector<TH1F*> hists, vector<TH1F*> ratHists, string type, bool hasErr){
  pair<double, double> minmax = getRange(hists, false);
  c2->cd();

	vector<TH1F*> ratioHists;

  for(unsigned int fit=0; fit<fitNames.size(); fit++){
      TH1F *hClone = (TH1F*)hists[fit]->Clone(Form("%sClone", hists[fit]->GetName()));
      hClone->Divide(ratHists[fit]);
      hClone->SetMarkerColor(TColor::GetColor(colors[fit]));
      hClone->SetLineColor(TColor::GetColor(colors[fit]));
      hClone->GetXaxis()->SetTitleSize(0.12);
      hClone->GetYaxis()->SetTitleSize(0.12);
      hClone->GetYaxis()->SetLabelSize(0.12);
      hClone->GetYaxis()->SetTitleOffset(0.30);
//      hClone->GetXaxis()->SetTitleOffset(0.30);
      hClone->GetXaxis()->SetLabelSize(0.12);
      hClone->GetYaxis()->SetNdivisions(4, 0, 2);
      hClone->GetYaxis()->SetTitle("Ratio to Area Sub.");
      ratioHists.push_back(hClone);
  }

  pair<double, double> minmaxRatio = getRange(ratioHists, true);

  for(unsigned int fit=0; fit<fitNames.size(); fit++){
		pad1->cd();
      hists[fit]->GetYaxis()->SetRangeUser(minmax.first, minmax.second*1.08);
      hists[fit]->GetXaxis()->SetLabelSize(0);
      hists[fit]->GetYaxis()->SetTitleSize(.05);
      hists[fit]->GetYaxis()->SetLabelSize(.045);
      hists[fit]->GetYaxis()->SetTitleOffset(0.9);
    	hists[fit]->GetYaxis()->SetRangeUser(minmax.first, minmax.second);
    if(hasErr == true){
      if(fit == 0)hists[fit]->Draw("Ex0C hist");
      else hists[fit]->Draw("Ex0C hist SAME");
      hists[fit]->Draw("LPhist SAME");
    }
    else{
      if(fit == 0)hists[fit]->Draw("LPhist");
      else hists[fit]->Draw("LPhist SAME");
    }

      pad2->cd();
			ratioHists[fit]->GetYaxis()->SetRangeUser(minmaxRatio.first, minmaxRatio.second);
      if(fit == 0) ratioHists[fit]->Draw("Ex0C hist");
      else ratioHists[fit]->Draw("Ex0C hist SAME");
      ratioHists[fit]->Draw("Lhist SAME");
			f->cd();
			ratioHists[fit]->Write();
  }

    pad1->cd();
    makeLegends(false);
    makeLabels(false);
    c2->Print(Form("%s.pdf", name.c_str()));

}


// Makes a vector of histograms
vector<TH1F*> calibration::makeHists(string histTitle,  string xAxis, string yAxis, int numBins, double minBin, double maxBin, int numHists, int colorBin, string vecName){
  vector<TH1F*> hists;
  for(int i=0; i<numHists; i++){
    int color = i;
    if(colorBin >= 0) color = colorBin;
		TH1F *tmp;
		if( strcmp(vecName.c_str(), "fits") == 0){
    	tmp = new TH1F(Form("h_%s_%s",fitNames[i].c_str(), histTitle.c_str()),  Form(" ;%s;%s", xAxis.c_str(), yAxis.c_str()), numBins, minBin, maxBin);
		}
		if( strcmp(vecName.c_str(), "NPV") == 0){
			double npvSpacing =  (maxNPV - minNPV )*1.0 / nNPVBins;
			double lowNPV = minNPV + npvSpacing * i;
			double highNPV = lowNPV + npvSpacing;
    	tmp = new TH1F(Form("h_%s_%s_%.0f_%.0f",histTitle.c_str(), vecName.c_str(), lowNPV, highNPV),  Form(" ;%s;%s", xAxis.c_str(), yAxis.c_str()), numBins, minBin, maxBin);
		}
    if( strcmp(vecName.c_str(), "Pt") == 0){
      double ptSpacing =  (maxPt - minPt )*1.0 / nPtBins;
      double lowPt = minPt + ptSpacing * i;
      double highPt = lowPt + ptSpacing;
      tmp = new TH1F(Form("h_%s_%s_%.0f_%.0f",histTitle.c_str(), vecName.c_str(), lowPt, highPt),  Form(" ;%s;%s", xAxis.c_str(), yAxis.c_str()), numBins, minBin, maxBin);
    }
    tmp->Sumw2();
    tmp->UseCurrentStyle();
    tmp->SetMarkerColor(TColor::GetColor(colors[color]));
    tmp->SetLineColor(TColor::GetColor(colors[color]));
    tmp->SetMarkerSize(1);
    hists.push_back(tmp);
  }
  return hists;
}


void calibration::drawIQR(TH1 *hist, double firstQuantile, double median, double thirdQuantile, double efficiency, int ptbin, int npvbin, int fit){
   hist->Draw("hist");
   TH1F *h1a = (TH1F*)hist->Clone(Form("%s_FirstQuantile", hist->GetName()));
   h1a->GetXaxis()->SetRangeUser(firstQuantile,median);
   h1a->SetFillColor(38);
 	 h1a->Draw("hist SAME");
   TH1F *h1b = (TH1F*)hist->Clone(Form("%s_ThirdQuantile", hist->GetName()));
   h1b->GetXaxis()->SetRangeUser(median, thirdQuantile);
   h1b->SetFillColor(46);
   h1b->Draw("hist SAME");
   hist->Draw("SAME");

  if(npvbin >= 0 && ptbin >= 0) makeLabels(npvbin, ptbin, true);
  else if(ptbin >= 0) makeLabels("pt", ptbin, true);
	double width = thirdQuantile-firstQuantile;
	double rhswidth = 2*(thirdQuantile-median);
  myText( 0.2, 0.66, 1, Form("Median: %2.3f", median));
  myText( 0.2, 0.62, 1, Form("RHS width: %2.3f", rhswidth));
  myText( 0.2, 0.58, 1, Form("Efficiency: %2.3f", efficiency));
  myMarkerText( 0.7, .88, TColor::GetColor(colors[fit]), 20, titleNames[fit].c_str(),1.3);
}


// Makes an array of histograms
vector<vector<TH1F*> > calibration::makeHistArray(string name, string xAxis, string yAxis, int numBins, double minBin, double maxBin, double nHists, string vecName){
  vector<vector<TH1F*> > hists(fitNames.size(), vector<TH1F*>(0)); 

  for(unsigned int fit=0; fit<fitNames.size(); fit++){
		hists[fit] = makeHists(Form("%s_%s", fitNames[fit].c_str(), name.c_str()), xAxis, yAxis, numBins, minBin, maxBin, nHists, fit, vecName);
	}

  return hists;
}




 
