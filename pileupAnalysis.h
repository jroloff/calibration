#include "TTree.h"
#include "TBranch.h"
#include "TLorentzVector.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <math.h>
#include "TROOT.h"
#include "TChain.h"
#include "TObject.h"
#include "TFile.h"


#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include "TApplication.h" 

#include "calibration.h"

using namespace std;

#ifdef __MAKECINT__
#pragma link C++ class std::vector<TLorentzVector>+;
#endif

#ifndef PILEUPANALYSIS_H
#define PILEUPANALYSIS_H

class pileupAnalysis{

private:
	TTree          *fChain;   //!pointer to the analyzed TTree or TChain
	//TChain          *fChain;   //!pointer to the analyzed TTree or TChain
	Int_t           fCurrent; //!current Tree number in a TChain

  int nPtBins;
  int minPt;
  int maxPt;
  int maxNPV;
	int minNPV;
  int nNPVBins;
  double  maxEta;
	double isolationSize;
	double efficiencyPtCut;
	double nSigma;

	int nResponseBins;
	double minResponse;
	double maxResponse;
	double isoPtCut;
	double truthIsoPtCut;

	double fracCutoff;
	int doEfficiency;
	int doFakeRate;
	string treename;
	string filename;
	int isTLorentz;
	int recoPtCut;
	int doRecoIso;	
	int doTruthIso;
	int hasFull4Mom;
	int isEM;
	double recoTruthMatchdR;
	int maxEntries;
	int doTruthIsolation;
	int doRecoIsolation;

	string inputdir;
  vector<string> fitNames;
  vector<string> titleNames;

  calibration *calib;//!

  TBranch *b_eventNPV;//!
  TBranch *b_eventMu;//!
  TBranch *b_eventNPVShort;//!
  TBranch *b_eventMuShort;//!
  TBranch *b_eventWeight;//!
  TBranch *b_eventWeightFloat;//!

	TBranch *b_truthJets;//!
	vector<TLorentzVector>* truthJets;//!

  vector<TBranch*> b_jets;//!
  vector<vector<TLorentzVector>* > jets;//!
	
  vector<TBranch*> b_truth;//!
  vector<vector<TLorentzVector>* > truth;//!

  TBranch *b_truthJetsPt;//!
  vector<float>* truthJetsPt;//!
  TBranch *b_truthJetsEta;//!
  vector<float>* truthJetsEta;//!
  TBranch *b_truthJetsPhi;//!
  vector<float>* truthJetsPhi;//!
  TBranch *b_truthJetsE;//!
  vector<float>* truthJetsE;//!

	vector<TBranch*> b_truthjetsMindR;//!
	vector<vector<float>* > truthjetsMindR;//!
	vector<TBranch*> b_jetsMindR;//!
	vector<vector<float>* > jetsMindR;//!

  vector<TBranch*> b_jetsPt;//!
  vector<vector<float>* > jetsPt;//!
  vector<TBranch*> b_jetsEta;//!
  vector<vector<float>* > jetsEta;//!
  vector<TBranch*> b_jetsPhi;//!
  vector<vector<float>* > jetsPhi;//!
  vector<TBranch*> b_jetsE;//!
  vector<vector<float>* > jetsE;//!

  vector<TBranch*> b_truthPt;//!
  vector<vector<float>* > truthPt;//!
  vector<TBranch*> b_truthEta;//!
  vector<vector<float>* > truthEta;//!
  vector<TBranch*> b_truthPhi;//!
  vector<vector<float>* > truthPhi;//!
  vector<TBranch*> b_truthE;//!
  vector<vector<float>* > truthE;//!

	
  //Int_t eventNPV;
  int eventNPV;
  Int_t eventMu;
  int npv;
  Int_t mu;

  //double eventWeight;
  double eventWeight;
  float eventWeightFloat;
  Long64_t tentry;
	Long64_t totalEntries;

  TTree *tree;
  TTree *tree2;
  TFile *dataFile;

	vector<double> getIsolation(vector<TLorentzVector> jetList, double ptCut);
	void fillTLorentzEntry( Long64_t jentry);
	void fillFlatEntry(Long64_t jentry);
	vector<double> getTruthIsolation(vector<TLorentzVector> *jetList,vector<TLorentzVector> *truthList, vector<TLorentzVector> *allTruthList, double minPt);
	void matchJetsToTruth();

public:

pileupAnalysis(int argc,char *argv[]);
~pileupAnalysis();

void fillTree(Long64_t jentry);
void Loop();
//virtual void     Init(TChain *tree);
virtual void     Init(TTree *tree);
};

#endif


#ifdef PILEUPANALYSIS_cxx


void pileupAnalysis::Init(TTree *tree){
  eventNPV = -99;
  eventMu = -99;
	eventWeight = -99;

	if(isTLorentz == 1) truthJets = 0;
	truthJetsPt = 0;
	truthJetsEta = 0;
	truthJetsPhi = 0;
	truthJetsE = 0;

  b_jets = vector<TBranch*>(fitNames.size());
  if(isTLorentz == 1) jets = vector<vector<TLorentzVector>* >(fitNames.size(),0);
  b_truth = vector<TBranch*>(fitNames.size());
  if(isTLorentz == 1) truth = vector<vector<TLorentzVector>* >(fitNames.size(),0);

  b_truthjetsMindR = vector<TBranch*>(fitNames.size());
  truthjetsMindR = vector<vector<float>* >(fitNames.size(),0);
  b_jetsMindR = vector<TBranch*>(fitNames.size());
  jetsMindR = vector<vector<float>* >(fitNames.size(),0);
  b_jetsPt = vector<TBranch*>(fitNames.size());
  jetsPt = vector<vector<float>* >(fitNames.size(),0);
  b_jetsEta = vector<TBranch*>(fitNames.size());
  jetsEta = vector<vector<float>* >(fitNames.size(),0);
  b_jetsPhi = vector<TBranch*>(fitNames.size());
  jetsPhi = vector<vector<float>* >(fitNames.size(),0);
  b_jetsE = vector<TBranch*>(fitNames.size());
  jetsE = vector<vector<float>* >(fitNames.size(),0);
  b_truthPt = vector<TBranch*>(fitNames.size());
  truthPt = vector<vector<float>* >(fitNames.size(),0);
  b_truthEta = vector<TBranch*>(fitNames.size());
  truthEta = vector<vector<float>* >(fitNames.size(),0);
  b_truthPhi = vector<TBranch*>(fitNames.size());
  truthPhi = vector<vector<float>* >(fitNames.size(),0);
  b_truthE = vector<TBranch*>(fitNames.size());
  truthE = vector<vector<float>* >(fitNames.size(),0);

   // Set branch addresses and branch pointers
  if (!tree) return;
  fChain = tree;
  fCurrent = -1;
  fChain->SetMakeClass(1);

  fChain->SetBranchAddress("eventNPV", &eventNPV, &b_eventNPV);
  fChain->SetBranchAddress("eventMu", &eventMu, &b_eventMu);
  fChain->SetBranchAddress("eventWeight", &eventWeight, &b_eventWeight);

	if(isTLorentz == false){
  	fChain->SetBranchAddress("NPV", &npv, &b_eventNPVShort);
  	fChain->SetBranchAddress("mu", &mu, &b_eventMuShort);
  	fChain->SetBranchAddress("event_weight", &eventWeightFloat, &b_eventWeightFloat);
	}
  fChain->SetBranchAddress("Truthjets", &truthJets, &b_truthJets);

	if(isTLorentz == false){
  	fChain->SetBranchAddress("tjpt", &truthJetsPt, &b_truthJetsPt);
  	fChain->SetBranchAddress("tjeta", &truthJetsEta, &b_truthJetsEta);
  	fChain->SetBranchAddress("tjphi", &truthJetsPhi, &b_truthJetsPhi);
  	fChain->SetBranchAddress("tjm", &truthJetsE, &b_truthJetsE);
	}

  for(unsigned int i=0; i < fitNames.size(); i++){
    fChain->SetBranchAddress(Form("%sjets", fitNames[i].c_str()), &jets[i], &b_jets[i]);
    fChain->SetBranchAddress(Form("%struth", fitNames[i].c_str()), &truth[i], &b_truth[i]);

		if(isTLorentz == false){
    fChain->SetBranchAddress(Form("tj%smindr", fitNames[i].c_str()), &truthjetsMindR[i], &b_truthjetsMindR[i]);
    fChain->SetBranchAddress(Form("j%smindr", fitNames[i].c_str()), &jetsMindR[i], &b_jetsMindR[i]);
    fChain->SetBranchAddress(Form("j%spt", fitNames[i].c_str()), &jetsPt[i], &b_jetsPt[i]);
    fChain->SetBranchAddress(Form("j%seta", fitNames[i].c_str()), &jetsEta[i], &b_jetsEta[i]);
    fChain->SetBranchAddress(Form("j%sphi", fitNames[i].c_str()), &jetsPhi[i], &b_jetsPhi[i]);
    fChain->SetBranchAddress(Form("j%smass", fitNames[i].c_str()), &jetsE[i], &b_jetsE[i]);

    fChain->SetBranchAddress(Form("tj%spt", fitNames[i].c_str()), &truthPt[i], &b_truthPt[i]);
    fChain->SetBranchAddress(Form("tj%seta", fitNames[i].c_str()), &truthEta[i], &b_truthEta[i]);
    fChain->SetBranchAddress(Form("tj%sphi", fitNames[i].c_str()), &truthPhi[i], &b_truthPhi[i]);
    fChain->SetBranchAddress(Form("tj%sm", fitNames[i].c_str()), &truthE[i], &b_truthE[i]);
		}
  }
}



#endif // #ifdef SKANALYSIS_cxx



