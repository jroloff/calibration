#include <iostream>
#include <vector>
#include <string>
#include <math.h>
#include <utility>

#include "TCanvas.h"
#include "TAxis.h"
#include "TLegend.h"
#include "TLorentzVector.h"
#include "TH1.h"
#include "TH2.h"
#include "TF1.h"
#include "TColor.h"
#include "TString.h"
#include "TGraphErrors.h"
#include "TROOT.h"
#include "TStyle.h"
#include "TFile.h"
#include "TPad.h"

#include "AtlasUtils.h"
#include "AtlasStyle.h"
#include "AtlasLabels.h"




using namespace std;

#ifndef CALIBRATION_H
#define CALIBRATION_H

class calibration{

private:

  vector<string> fitNames; 
  vector<string> titleNames; 
	vector<TString> colors;

	string responseString;
	string resolutionString;
	string iqrString;
	string aiqrString;
 
	double nentries;
	double sumWeights;
	double nSigma;

	unsigned int nNPVBins;
	int minNPV;	
	int maxNPV;

	unsigned int nPtBins;
	int minPt;	
	int maxPt;
	int isEM;

	unsigned int nResponseBins;
	double minResponse;
	double maxResponse;
	double efficiencyCalibPtCut;
	double fracCutoff;
	double isoPtCut;

	TFile *f;

  vector<vector<vector<vector<double> > > > dataPt;
  vector<vector<vector<vector<double> > > > dataPtTrue;
  vector<vector<vector<vector<double> > > > dataWeight;
  vector<vector<vector<vector<bool> > > > dataIsolation;

  vector<vector<vector<vector<double> > > > NonIsoDataPt;
  vector<vector<vector<vector<double> > > > NonIsoDataPtTrue;
  vector<vector<vector<vector<double> > > > NonIsoDataWeight;

  vector<vector<vector<double> > > fakeDataPt;
  vector<vector<vector<double> > > fakeDataWeight;
  vector<vector<vector<double> > > truthDataWeight;

  vector<vector<vector<double> > > fakeDataPtTwo;
  vector<vector<vector<double> > > fakeDataWeightTwo;
  vector<vector<vector<double> > > truthDataWeightTwo;

  vector<vector<vector<double> > > allDataPt;
  vector<vector<vector<double> > > allDataWeight;
  vector<vector<vector<double> > > allDataPtTwo;
  vector<vector<vector<double> > > allDataWeightTwo;

  vector<vector<vector<double> > > avgPtTrue;
  vector<vector<vector<double> > > errAvgPtTrue;
  vector<vector<vector<double> > > avgResponse;
  vector<vector<vector<double> > > errAvgResponse;
  vector<vector<vector<double> > > avgNonIsoResponse;
  vector<vector<vector<double> > > errAvgNonIsoResponse;

  vector<vector<vector<vector<double> > > > truthMatchedPt;
  vector<vector<vector<vector<double> > > > truthMatchedReco;
  vector<vector<vector<vector<double> > > > truthMatchedWeight;
  vector<vector<vector<double> > > truthAllPt;
  vector<vector<vector<double> > > truthAllWeight;
  vector<vector<double> > truthAllWeightInclusive;
  vector<vector<vector<double> > > truthMatchedRatio;

  vector<vector<vector<vector<double> > > > truthMatchedPtTwo;
  vector<vector<vector<vector<double> > > > truthMatchedRecoTwo;
  vector<vector<vector<vector<double> > > > truthMatchedWeightTwo;
  vector<vector<vector<double> > > truthAllPtTwo;
  vector<vector<vector<double> > > truthAllWeightTwo;
  vector<vector<vector<double> > > truthMatchedRatioTwo;

  vector<vector<vector<double> > > NonIsoTruthWeight;
  vector<vector<vector<double> > > NonIsoTruthMatchedWeight;
  vector<vector<double> > NonIsoTruthWeightInclusive;
  vector<vector<double> > NonIsoTruthMatchedWeightInclusive;

	vector<double> sumWeightNPV;
	vector<double> sumWeightNPVTwo;

	vector<vector<TF1*> > RecoFits;
	vector<vector<TF1*> > RespFits;

	string path;
	TCanvas *c1;
	TCanvas *c2;
	TPad *pad1;
	TPad *pad2;

private:
 	double g1(double ptTrue, TF1 *Resp);


	void drawIQR(TH1 *hist, double firstQuantile, double median, double thirdQuantile, double efficiency, int ptbin, int npvbin, int fit);

	// Returns 
  double quantile(vector<double> data, vector<double> weights, double mean, double quantile);
  double getRHSVariance(vector<double> data, vector<double> weights, double mean);
  double getRHSWeightSq(vector<double> data, vector<double> weights, double mean);
  double getWeightSq(vector<double> data, vector<double> weights, double mean);
	double average(vector<double> data, vector<double> weights);
	double absoluterhsiqr(vector<double> data, vector<double> weights, double totalWeight);
	double absoluterhsiqr(vector<double> data, vector<double> weights, double totalWeight, double quantile, double fitMean);
	double absolutelhsiqr(vector<double> data, vector<double> weights, double totalWeight, double quantile);
	double absoluteefficiency(vector<double> data, vector<double> weights, double totalWeight);

  TF1* getFit(string filename, TH1* hist, double nSigmaB = 1, double nSigmaA = 1, int fit=0, int ptbin = -1, int npvbin = -1);
	//Makes vector or array of histograms
  vector<vector<TH1F*> > makeHistArray(string name, string xAxis, string yAxis, int numBins, double minBin, double maxBin, double nHists, string vecName);
  vector<TH1F*> makeHists(string histTitle,  string xAxis, string yAxis, int numBins, double minBin, double maxBin, int numHists, int colorBin = -1, string vecName = "");

	//Returns an optimal range for printing the histograms so that similar histograms for different algorithms have the same range
  pair<double,double> getRange(vector<TH1F*> hists, bool isRatioPlot);
  vector<pair<double,double> > getRange(vector<vector<TH1F*> > hists, bool isRatioPlot);

	//Draws the hists
  void drawHists(string name, vector<TH1F*> hists, string type, bool hasErr=true);
  void drawRatio(string name, vector<TH1F*> hists, vector<TH1F*> ratHists, string type, bool hasErr=true);
  void drawHists(string name, vector<vector<TH1F*> > hists, string type, bool hasErr=true);
  void drawRatio(string name, vector<vector<TH1F*> > hists, vector<vector<TH1F*> > ratHists, string type, bool hasErr=true);
	double getVariance(vector<double> data, vector<double> weights, double median);

	//Fills hists with the data (in both pt and npv if applicable) and then prints them
  void printHists(string name, vector<TH1F*> histsPt, vector<vector<double> > data, string yAxis);
  void printHists(string name, vector<vector<double> > data, string type, string yAxis, vector<vector<double> > err);
  void printRatio(string name, vector<vector<double> > data, string type, string yAxis, vector<vector<double> > err, unsigned int fitRat);
  void printHists(string name, vector<vector<vector<double> > > data, string yAxis, vector<vector<vector<double> > > err);
  void printRatio(string name, vector<vector<vector<double> > > data, string yAxis, vector<vector<vector<double> > > err, unsigned int fitRat);

  vector<int> getIsolation(vector<double> ptList, vector<TLorentzVector> jetList, double ptMin);
	//Labels histograms
  void makeLabels(string datatype, int databin, bool doSmall);
  void makeLabels(int npvbin, int ptbin, bool doSmall);
  void makeLabels(bool doSmall);
  void makeLegends(bool doSmall);
  void makeColors();

public:

  void makeCalibratedPlots(string calibType, bool isCalib);
  void makeCalibrationCurves();
	void doEfficiency();
	void doFakes();
	void finalize();

  calibration(vector<string> names, vector<string> titles, double nevt, unsigned int npvbins, double minnpv, double maxnpv, unsigned int ptbins, double minpt, double maxpt, double efficiencyCut, int nresponsebins, double minresponse, double maxresponse, int isem, double nsigma, double fracCutoff, double isoptcut);
  ~calibration();

  void fillData(int n, float ptReco, float ptTruth,  float npv, float eventWeight, bool isIsolated);
  void fillFakeData(int n, double ptRatio, int npv, double eventWeight, bool isMatched);
 
  void fillTruthData(int n, double ptReco, double ptTrue, int npv, double eventWeight, bool isMatched, bool isIsolated);
	void fillEntryNPV(double npv, double eventWeight, int nTruth);

	void doCalibration();


};



#endif
