CC = g++
CFLAGS = -c -g -Wall `root-config --cflags`
LDFLATS =`root-config --glibs`
GLIBS = $(shell root-config --glibs)
OBJS = runAnalysis.o pileupAnalysis.o calibration.o 

ATLASOBJS = AtlasLabels.o AtlasStyle.o AtlasUtils.o 
RootClass=myClass.h
RootDict=Dict.cpp
RootLinkDef=LinkDef.h 
INC = -l

#read : readHists.o $(ATLASOBJS)
#	$(CC) $(LDFLATS) readHists.o $(ATLASOBJS) -o read

main : $(OBJS) $(ATLASOBJS)
	$(CC)  $(LDFLATS) $(OBJS) $(ATLASOBJS)  -o main

AtlasStyle.o: AtlasStyle.C
	$(CC) $(CFLAGS) AtlasStyle.C $<

AtlasUtils.o: AtlasUtils.C
	$(CC) $(CFLAGS) AtlasUtils.C $<

AtlasLabels.o: AtlasLabels.C
	$(CC) $(CFLAGS) AtlasLabels.C $<

runAnalysis.o: runAnalysis.cxx $<
	$(CC) $(CFLAGS) runAnalysis.cxx

pileupAnalysis.o: pileupAnalysis.cxx $<
	$(CC) $(CFLAGS) pileupAnalysis.cxx

calibration.o: calibration.cxx 
	$(CC) $(CFLAGS) calibration.cxx $<

$(RootDict): $(RootClass) $(RootLinkDef)
	rootcint -v4 -f $(RootDict) -c $(INC) $(RootClass) $(RootLinkDef)

clean:
	rm *o main


