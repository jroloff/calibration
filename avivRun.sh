#/bin/bash

./main --filename avivData.root \
	--treename oTree \
	--input voro0 \
	--inputname Voronoi \
	--input voro1 \
	--inputname Voronoi_1Sigma \
	--minNPV  5 \
	--maxNPV 25 \
	--nNPVBins 4 \
	--nResponseBins 60 \
	--minResponse 0 \
	--maxResponse 2 \
	--nSigma 1.75 \
	--fracCutoff 0.1 \
	--doEfficiency 0 \
	--doFakeRate 0 \
	--isTLorentz 0 \
	--isoPtCut 2 \
	--doTruthIso 1 \
	--truthIsoPtCut 5 \
	--isEM 1 \
	--efficiencyPtCut 20 
