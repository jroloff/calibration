#ifndef PILEUPANALYSIS_cxx
#define PILEUPANALYSIS_cxx
#include <unistd.h>
#include "pileupAnalysis.h"
#include "TDirectory.h"
#include <getopt.h> 
#include "TFileCollection.h"
#include "TROOT.h"
#include "TDirectory.h"
#include "TSystem.h"

using namespace std;

pileupAnalysis::pileupAnalysis(int argc,char *argv[]){
	minPt = 20;
	maxPt = 60;
	nPtBins = 8;

	minNPV = 5;
	maxNPV = 25;
	nNPVBins = 4;

	minResponse = 0;
	maxResponse = 2;
	nResponseBins = 60;

	maxEta = 0.8;
	isolationSize = 0.6;

	efficiencyPtCut = 99999;

	doEfficiency = 1;
	doFakeRate = 0;
	doTruthIsolation = 1;
	doRecoIsolation = 0;
	truthIsoPtCut = 5;
	isoPtCut = 2;
	
	isTLorentz = 1;
	maxEntries = -1;
	
	treename = "nominal";
	filename = "lowMu.root";
	
	fracCutoff = 0;



static struct option long_options[] =
{
    {"minPt", 1, NULL, 'a'},
    {"maxPt", 1, NULL, 'b'},
    {"nPtBins", 1, NULL, 'c'},
    {"minNPV", 1, NULL, 'd'},
    {"maxNPV", 1, NULL, 'e'},
    {"nNPVBins", 1, NULL, 'f'},
    {"maxEta", 1, NULL, 'g'},
    {"isolationSize", 1, NULL, 'h'},
    {"minResponse", 1, NULL, 'i'},
    {"doEfficiency", 1, NULL, 'j'},
    {"isTLorentz", 1, NULL, 'k'},
    {"nResponseBins", 1, NULL, 'l'},
    {"maxResponse", 1, NULL, 'm'},
    {"isEM", 1, NULL, 'n'},
    {"treename", 1, NULL, 'o'},
		{"isoPtCut", 1, NULL, 'p'},
    {"filename", 1, NULL, 'q'},
    {"truthIsoPtCut", 1, NULL, 'r'},
		{"input", 1, NULL, 's'},
		{"inputname", 1, NULL, 't'},
		{"maxentries", 1, NULL, 'u'},
		{"doFakeRate", 1, NULL, 'v'},
		{"efficiencyPtCut", 1, NULL, 'w'},
		{"nSigma", 1, NULL, 'x'},
		{"fracCutoff", 1, NULL, 'y'},
		{"doTruthIsolation", 1, NULL, 'z'},
    {NULL, 0, NULL, 0}
};


	int opt;
  while ( (opt = getopt_long(argc, argv,"abcdefghijkopqrstuvwxyz", long_options, NULL)) != -1 ) {  // for each option...
    switch ( opt ) {
			case 'a': minPt = atoi(optarg); break;
			case 'b': maxPt = atoi(optarg); break;
			case 'c': nPtBins = atoi(optarg); break;
			case 'd': minNPV = atoi(optarg); break;
			case 'e': maxNPV = atoi(optarg); break;
			case 'f': nNPVBins = atoi(optarg); break;
			case 'g': maxEta = atof(optarg); break;
			case 'h': isolationSize = atof(optarg); break;
			case 'i': minResponse = atof(optarg); break;
			case 'j': doEfficiency = atoi(optarg); break;
			case 'k': isTLorentz = atoi(optarg); break;
			case 'l': nResponseBins = atoi(optarg); break;
			case 'm': maxResponse = atof(optarg); break;
			case 'n': isEM = atoi(optarg); break;
			case 'o': treename = optarg; break;
			case 'p': isoPtCut = atof(optarg); break;
			case 'q': filename = optarg; break;
			case 'r': truthIsoPtCut = atof(optarg); break;
			case 's': fitNames.push_back(optarg); break;
		 	case 't':	titleNames.push_back(optarg); break;
			case 'u': maxEntries = atoi(optarg); break;
			case 'v': doFakeRate = atoi(optarg); break;
			case 'w': efficiencyPtCut = atof(optarg); break;
			case 'x': nSigma = atof(optarg); break;
			case 'y': fracCutoff = atof(optarg); break;
			case 'z': doTruthIsolation = atoi(optarg); break;
			case 0: break;
    }
  }

	if(fitNames.size() != titleNames.size()){
		titleNames.clear();
		for(unsigned int i=0; i<fitNames.size(); i++){
			titleNames.push_back(fitNames[i]);
		}
	}

  truthJets = new vector<TLorentzVector>();
  jets = vector<vector<TLorentzVector>* >(fitNames.size(), 0);
  truth = vector<vector<TLorentzVector>* >(fitNames.size(), 0);
	for(int i=0; i<fitNames.size(); i++){
		jets[i] = new vector<TLorentzVector>;
		truth[i] = new vector<TLorentzVector>;
	}

	totalEntries = 0;	

	calib = new calibration(fitNames, titleNames, totalEntries, nNPVBins, minNPV, maxNPV, nPtBins, minPt, maxPt, efficiencyPtCut, nResponseBins, minResponse, maxResponse, isEM, nSigma, fracCutoff, isoPtCut);
	
	string line;
  fstream infile(filename.c_str());
	while (std::getline(infile, line)){
    string mcFile = line;
		cout << mcFile << endl;
    TFile *file = (TFile*)gROOT->GetListOfFiles()->FindObject(mcFile.c_str());
    if (!file || !file->IsOpen()) file = new TFile(mcFile.c_str());
  	file->GetObject(treename.c_str(),tree);
  	Init(tree);
		Loop();
  }

/*
    TFile *file = (TFile*)gROOT->GetListOfFiles()->FindObject(filename.c_str());
    if (!file || !file->IsOpen()) file = new TFile(filename.c_str());
    file->GetObject(treename.c_str(),tree);
    Init(tree);
    Loop();
*/

  calib->doCalibration();
  if(doEfficiency == 1) calib->doEfficiency();
  if(doFakeRate == 1) calib->doFakes();

	calib->finalize();
	cout << "Total entries: " << totalEntries << endl;
}


void pileupAnalysis::Loop(){
  if (fChain == 0) return;

  Long64_t nentries = fChain->GetEntriesFast();
	int maxTotalEntries = 999999999;
//	if(maxEntries < 0) maxEntries = nentries;
	if(maxEntries > 0) maxTotalEntries = maxEntries;


  for (Long64_t jentry = 0; jentry < nentries && totalEntries < maxTotalEntries; jentry++) {
    if(jentry%1000 == 0) cout << "Processed " << totalEntries << " Events" << endl;	
    fillTree(jentry);
		totalEntries++;

		if(eventNPV < minNPV || eventNPV >= maxNPV) continue;
		
		int nTruth = 0; // Normalization for fakes

    //Fill the truth jet information  
    if(doEfficiency == 1 || doFakeRate == 1){
      for(unsigned int k=0; k < truthJets->size(); k++){
        double trueEta = (*truthJets)[k].Eta();
        double truePt = (*truthJets)[k].Pt();
        if( fabs(trueEta) > maxEta)  continue;
        if(truePt < minPt || truePt > maxPt) continue;
        nTruth++;
			}
			calib->fillEntryNPV(eventNPV, eventWeight, nTruth);
		}

    for(unsigned int fit=0; fit < fitNames.size(); fit++){
			vector<double> isolationVec;
			if(isTLorentz == 1 && doRecoIsolation == 1) isolationVec = getIsolation((*jets[fit]), isoPtCut);
			vector<double> truthIsolationVec;
			if(isTLorentz == 1 && doTruthIsolation == 1) truthIsolationVec = getTruthIsolation((jets[fit]), (truth[fit]), truthJets, 0.0001);
				
			//Fill the truth jet information	
			vector<double> truthIsoVec;
			truthIsoVec = getIsolation(*truthJets, truthIsoPtCut);

	    for(unsigned int k=0; k < truthJets->size(); k++){
				double trueEta, truePt;
				double recoPt = 0;
				trueEta = (*truthJets)[k].Eta();
   	    truePt = (*truthJets)[k].Pt();
				if( fabs(trueEta) > maxEta)	continue;
				if(truePt < minPt) continue;

				bool isMatched = false;
				bool isTruthIsolated = true;
				if(doTruthIsolation == 1 && truthIsoVec[k] < isolationSize) isTruthIsolated = false;
				if(recoPt < 0.0001) continue;

	      for(unsigned int m=0; m < truth[fit]->size(); m++){
					if((*jets[fit])[m].DeltaR((*truthJets)[k]) > 0.3) continue;
					double matchedPt = (*jets[fit])[m].Pt();	
					//if(isTLorentz == 1 && fit >= 0)		matchedPt = matchedPt / 1000.;	

	        if( matchedPt > recoPt){
						isMatched = true;
						recoPt = matchedPt;
					}
    	  }
  	    calib->fillTruthData(fit, recoPt, truePt, eventNPV, eventWeight, isMatched, isTruthIsolated);
	    }


      //Fill the reco jet information (matched and fake)
      for(unsigned int k=0; k<jets[fit]->size(); k++){
        double ptTrue = (*truth[fit])[k].Pt();
        double ptReco = (*jets[fit])[k].Pt();
				//if(isTLorentz == 1 && fit >= 0)		ptReco = ptReco / 1000.;	
        double deltaR =  (*jets[fit])[k].DeltaR((*truth[fit])[k]);
        double etaTrue = (*truth[fit])[k].Eta();
        double etaReco = (*jets[fit])[k].Eta();
				bool isIsolated = true;
				bool isMatched = true;

				if( ptTrue < 4 || deltaR > isolationSize) isMatched = false;
				isMatched = false;

				if(isMatched == false && fabs(etaReco) < 0.8){
					for(int truth = 0; truth < truthJets->size(); truth++){
						if( (*jets[fit])[k].DeltaR((*truthJets)[truth]) < isolationSize && (*truthJets)[truth].Pt() > 5) 	isMatched = true;
					}
				}

        if(fabs(etaReco) < maxEta && doFakeRate == true) calib->fillFakeData(fit, ptReco, eventNPV, eventWeight, isMatched);

				if(fabs(etaTrue) > maxEta) continue;

				if(isTLorentz == 1){
        	if(doRecoIsolation == 1 && isolationVec[k] < 0.6) isIsolated = false;
        	if(doTruthIsolation == 1 && truthIsolationVec[k] < isolationSize) isIsolated = false;
				}
				else{
					if(doRecoIsolation == 1 && (*jetsMindR[fit])[k] < 0.6) isIsolated = false;
					if(doTruthIsolation == 1 && (*truthjetsMindR[fit])[k] < isolationSize) isIsolated = false;
				}

				if(ptReco < 0.0001) continue;

        if(ptTrue > 4 && deltaR < isolationSize/2. && isIsolated == true){
        	calib->fillData(fit, ptReco, ptTrue, eventNPV, eventWeight, isIsolated);
				}
      } //End jet loop
    } //Get all info for different types of subtraction
  } //Loop through events

}

pileupAnalysis::~pileupAnalysis(){

}


//Returns a vector of the positions of jets that don't pass isolation
//Only considers nearby jets with pT > minPt
vector<double> pileupAnalysis::getIsolation(vector<TLorentzVector> jetList, double ptCut){
  unsigned int jetSize = jetList.size();
  vector<double> isolationVec(jetSize,10);

  for(unsigned int i=0; i<jetSize; i++){
		if(fabs(jetList[i].Eta()) > 1.4) continue;
    for(unsigned int j=0; j<jetSize; j++){
			if(i == j) continue;
			if( jetList[i].Pt() == jetList[j].Pt() && jetList[i].Eta() == jetList[j].Eta()) continue;
      double dR = jetList[i].DeltaR( jetList[j]);
			if(jetList[j].Pt() < ptCut) continue;

      if( dR < isolationVec[i] ){
        isolationVec[i] = dR;
      }
    }

  }
  return isolationVec;
}


vector<double> pileupAnalysis::getTruthIsolation(vector<TLorentzVector> *jetList,vector<TLorentzVector> *truthList, vector<TLorentzVector> *allTruthList, double minPt){
	unsigned int jetSize = jetList->size();
	unsigned int allTruthSize = allTruthList->size();
	vector<double> isolationVec(jetSize, 10);
 
   for(unsigned int i=0; i<jetSize; i++){
 		if( (*truthList)[i].M() < 0.1) continue;
 
 		//Ignore any jets who are matched to non-isolated truth jets
 		for(unsigned int j=0; j<allTruthSize; j++){
			if( (*allTruthList)[j].Pt() < truthIsoPtCut) continue;
 			if( (*truthList)[i].Eta() == (*allTruthList)[j].Eta() &&  (*truthList)[i].Phi() == (*allTruthList)[j].Phi() ) continue;
     	double dR = (*truthList)[i].DeltaR( (*allTruthList)[j]);
      if( dR < isolationVec[i]){
			  isolationVec[i] = dR;
			}
 		}

		if(isolationVec[i] < isolationSize) continue;

		//If two jets are matched to the same truth, choose the one with higher $p_T$
    for(unsigned int j=i+1; j<jetSize; j++){
			if( (*truthList)[j].E() < 0.001) continue;

			if( (*truthList)[i].Eta() == (*truthList)[j].Eta() && (*truthList)[i].Phi() == (*truthList)[j].Phi()) {
				if( (*jetList)[i].Pt() < (*jetList)[j].Pt() ) isolationVec[i] = 0;
        else isolationVec[j] = 0;
			}
    }
  }
  return isolationVec;
}


void pileupAnalysis::fillTree(Long64_t jentry){
  if (fChain == 0) return;
	if(isTLorentz == 1) fillTLorentzEntry(jentry);
	else fillFlatEntry(jentry);
}

void pileupAnalysis::fillTLorentzEntry( Long64_t jentry){
  b_eventNPV->GetEntry(jentry);
  b_eventMu->GetEntry(jentry);
  b_eventWeight->GetEntry(jentry);
  b_truthJets->GetEntry(jentry);

  for(unsigned int j = 0; j < fitNames.size(); j++){
    b_jets[j]->GetEntry(jentry);
    b_truth[j]->GetEntry(jentry);
  }
}

//Fills branches and converts to TLorentzVectors
void pileupAnalysis::fillFlatEntry(Long64_t jentry){
  	b_eventWeightFloat->GetEntry(jentry);
		eventWeight = eventWeightFloat;
  	b_eventNPVShort->GetEntry(jentry);
  	b_eventMuShort->GetEntry(jentry);
		eventNPV = npv;
		eventMu = mu;

	  b_truthJetsPt->GetEntry(jentry);
  	b_truthJetsEta->GetEntry(jentry);
	  b_truthJetsPhi->GetEntry(jentry);
  	b_truthJetsE->GetEntry(jentry);

		truthJets->clear();


		for(unsigned int i=0; i<truthJetsPt->size(); i++){
			if( (*truthJetsPt)[i] < 0.00001) continue;
      TLorentzVector tmpJet(0,0,0,0);
      tmpJet.SetPtEtaPhiM( (*truthJetsPt)[i], (*truthJetsEta)[i], (*truthJetsPhi)[i], (*truthJetsE)[i]);
      truthJets->push_back(tmpJet);
		}

  for(unsigned int j = 0; j < fitNames.size(); j++){
    b_truthjetsMindR[j]->GetEntry(jentry);
    b_jetsMindR[j]->GetEntry(jentry);

    b_jetsPt[j]->GetEntry(jentry);
    b_jetsEta[j]->GetEntry(jentry);
	  b_jetsPhi[j]->GetEntry(jentry);
    b_jetsE[j]->GetEntry(jentry);

    b_truthPt[j]->GetEntry(jentry);
    b_truthEta[j]->GetEntry(jentry);
  	b_truthPhi[j]->GetEntry(jentry);
	  b_truthE[j]->GetEntry(jentry);
  }

	for(int i=0; i<fitNames.size(); i++){
		jets[i]->clear();
		truth[i]->clear();
	}

	for(unsigned int i = 0; i < fitNames.size(); i++){
		for(unsigned int j=0; j<jetsPt[i]->size(); j++){
			if( (*truthPt[i])[j] < 0.00001){
        (*truthPt[i])[j] = 0.01;
        (*truthEta[i])[j] = 0.01;
        (*truthPhi[i])[j] = 0.01;
        (*truthE[i])[j] = 0.01;
			}

			TLorentzVector tmpJet(0,0,0,0);
			tmpJet.SetPtEtaPhiM( (*jetsPt[i])[j], (*jetsEta[i])[j], (*jetsPhi[i])[j], (*jetsE[i])[j]);
			if( (*jetsPt[i])[j] < 0 ) cout << "negative" << endl;

			jets[i]->push_back(tmpJet);
			TLorentzVector tmpTruth(0,0,0,0);

			tmpTruth.SetPtEtaPhiM( (*truthPt[i])[j], (*truthEta[i])[j], (*truthPhi[i])[j], (*truthE[i])[j]);
			truth[i]->push_back(tmpTruth);
		}
	}
}




#endif
